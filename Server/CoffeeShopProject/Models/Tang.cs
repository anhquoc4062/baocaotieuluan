﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoffeeShopProject.Models
{
    public partial class Tang
    {
        public int MaTang { get; set; }
        public string TenTang { get; set; }
    }
}
