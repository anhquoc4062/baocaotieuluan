﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoffeeShopProject.Models
{
    public class PushDevice
    {
        public int PushID { get; set; }
        public string Token { get; set; }
    }
}
