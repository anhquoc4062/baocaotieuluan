package fpo.thanh.coffeeshop.domain.interactors.impl;

import fpo.thanh.coffeeshop.domain.executor.Executor;
import fpo.thanh.coffeeshop.domain.executor.MainThread;
import fpo.thanh.coffeeshop.domain.executor.impl.ThreadExecutor;
import fpo.thanh.coffeeshop.domain.interactors.LoginInteractor;
import fpo.thanh.coffeeshop.domain.interactors.base.AbstractInteractor;
import fpo.thanh.coffeeshop.domain.repository.LoginRepository;
import fpo.thanh.coffeeshop.shareModel.LoginModel;
import fpo.thanh.coffeeshop.threading.MainThreadImpl;

public class LoginInteractorImpl extends AbstractInteractor implements LoginInteractor {
    private final Callback callback;
    private final LoginRepository repository;
    private final String username;
    private final String password;

    public LoginInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback callback, LoginRepository repository,
                               String username, String password) {
        super(threadExecutor, mainThread);
        this.callback = callback;
        this.repository = repository;
        this.username = username;
        this.password = password;
    }

    @Override
    public void run() {
        final LoginModel result=repository.login(username,password);
        mMainThread.post(()->callback.onFinish(result));
    }
}
