package fpo.thanh.coffeeshop.presentation.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import fpo.thanh.coffeeshop.R;
import fpo.thanh.coffeeshop.domain.executor.impl.ThreadExecutor;
import fpo.thanh.coffeeshop.domain.repository.Impl.LoginRepositoryImpl;
import fpo.thanh.coffeeshop.presentation.presenter.LoginPresenter;
import fpo.thanh.coffeeshop.presentation.presenter.impl.LoginPresenterImpl;
import fpo.thanh.coffeeshop.shareModel.LoginModel;
import fpo.thanh.coffeeshop.storage.savePreference.CacheClient;
import fpo.thanh.coffeeshop.threading.MainThreadImpl;

public class LoginActivity extends AppCompatActivity implements LoginPresenter.View {
    @BindView(R.id.edt_username)
    TextInputEditText edt_username;
    @BindView(R.id.edt_password)
    TextInputEditText edt_password;
    @BindView(R.id.btn_login)
    Button btn_login;
    LoginPresenter presenter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        Log.e("checkLogin",CacheClient.getCache(this,"checkLogin")+"--");
        if(CacheClient.getCache(this,"checkLogin").equals("2")){
            startActivity(new Intent(this,SplashScreen.class));
            this.finish();
        }
        presenter=new LoginPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(),this,this,new LoginRepositoryImpl());
        btn_login.setOnClickListener(v->{
            presenter.requestLogin(edt_username.getText().toString(),edt_password.getText().toString());
        });
    }

    @Override
    public void showResultLogin(LoginModel result) {
        try{
            if(result.getExitCode()==1){
                CacheClient.setCache(this,"tokenfirebase","fI0b7A9SDGE:APA91bHgISu4Qy94o9ieC2UVxO9OVOdaDIrcXM15JVI4_bTOm7fKjApyLm8RZrcy1MajCMBB-WQmY9xMYF7KvIfpNhI7TffSND4liszSyIs29Fn17ip7wZP5TJWFrK-JwUyXo33L7pU0");
                CacheClient.setCache(this,"username",result.getData().getTenTaiKhoan());
                CacheClient.setCache(this,"anhDaiDien",result.getData().getAnhDaiDien());
                CacheClient.setCache(this,"checkLogin","2");
                CacheClient.setCache(this,"maNhanVien",String.valueOf(result.getData().getMaTaiKhoan()));
                startActivity(new Intent(this,SplashScreen.class));
                this.finish();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
