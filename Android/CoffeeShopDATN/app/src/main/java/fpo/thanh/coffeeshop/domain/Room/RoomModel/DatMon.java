package fpo.thanh.coffeeshop.domain.Room.RoomModel;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class DatMon {
    @PrimaryKey(autoGenerate = true)
    public Integer maDsMon;
    @ColumnInfo(name = "maBan")
    public Integer maBan;
    @ColumnInfo(name = "maThucDon")
    public Integer maThucDon;
    @ColumnInfo(name = "tenThucDon")
    public String tenThucDon;
    @ColumnInfo(name = "ngayDatMon")
    public String ngayDatMon;
    @ColumnInfo(name = "soLuongMon")
    public Integer soLuongMon;
}
