package fpo.thanh.coffeeshop.contanst;

import android.widget.TextView;

public class STATUS_ORDER {
    public static String STATUS_ORDER(int s){
        switch (s){
            case 1: return "Hóa đơn mới";
            case 2: return "Đã cập nhật";
            case 3: return "Đã hủy";
            case 4: return "Change Ignore";
            case 5: return "Gộp";
            case 6: return "Đang chế biến";
            case 7: return "Đã trả món";
            case 8: return "Đã thanh toán";
            default:return "Không xác định";
        }
    }
}
