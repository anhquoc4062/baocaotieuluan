package fpo.thanh.coffeeshop.shareModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataLoginModel {
    @SerializedName("maTaiKhoan")
    @Expose
    private Integer maTaiKhoan;
    @SerializedName("tenTaiKhoan")
    @Expose
    private String tenTaiKhoan;
    @SerializedName("matKhau")
    @Expose
    private String matKhau;
    @SerializedName("maPhanQuyen")
    @Expose
    private String maPhanQuyen;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("anhDaiDien")
    @Expose
    private String anhDaiDien;
    @SerializedName("dangHoatDong")
    @Expose
    private Integer dangHoatDong;
    @SerializedName("maPhanQuyenNavigation")
    @Expose
    private Object maPhanQuyenNavigation;
    @SerializedName("khachHang")
    @Expose
    private List<Object> khachHang = null;
    @SerializedName("nhanVien")
    @Expose
    private List<Object> nhanVien = null;

    public Integer getMaTaiKhoan() {
        return maTaiKhoan;
    }

    public void setMaTaiKhoan(Integer maTaiKhoan) {
        this.maTaiKhoan = maTaiKhoan;
    }

    public String getTenTaiKhoan() {
        return tenTaiKhoan;
    }

    public void setTenTaiKhoan(String tenTaiKhoan) {
        this.tenTaiKhoan = tenTaiKhoan;
    }

    public String getMatKhau() {
        return matKhau;
    }

    public void setMatKhau(String matKhau) {
        this.matKhau = matKhau;
    }

    public String getMaPhanQuyen() {
        return maPhanQuyen;
    }

    public void setMaPhanQuyen(String maPhanQuyen) {
        this.maPhanQuyen = maPhanQuyen;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAnhDaiDien() {
        return anhDaiDien;
    }

    public void setAnhDaiDien(String anhDaiDien) {
        this.anhDaiDien = anhDaiDien;
    }

    public Integer getDangHoatDong() {
        return dangHoatDong;
    }

    public void setDangHoatDong(Integer dangHoatDong) {
        this.dangHoatDong = dangHoatDong;
    }

    public Object getMaPhanQuyenNavigation() {
        return maPhanQuyenNavigation;
    }

    public void setMaPhanQuyenNavigation(Object maPhanQuyenNavigation) {
        this.maPhanQuyenNavigation = maPhanQuyenNavigation;
    }

    public List<Object> getKhachHang() {
        return khachHang;
    }

    public void setKhachHang(List<Object> khachHang) {
        this.khachHang = khachHang;
    }

    public List<Object> getNhanVien() {
        return nhanVien;
    }

    public void setNhanVien(List<Object> nhanVien) {
        this.nhanVien = nhanVien;
    }
}
