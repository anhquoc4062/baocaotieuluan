package fpo.thanh.coffeeshop.presentation.presenter;

import fpo.thanh.coffeeshop.shareModel.GetOrderModel;

public interface GetOrderPresenter {
    interface View{
        void showResultGetOrder(GetOrderModel result);
    }
    void requestGetOrder(String id);
}
