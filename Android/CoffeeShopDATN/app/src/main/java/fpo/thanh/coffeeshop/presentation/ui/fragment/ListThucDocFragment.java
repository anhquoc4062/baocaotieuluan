package fpo.thanh.coffeeshop.presentation.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fpo.thanh.coffeeshop.R;
import fpo.thanh.coffeeshop.contanst.LoaiThucDon;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.ThucDon;
import fpo.thanh.coffeeshop.presentation.model.CountBadge;
import fpo.thanh.coffeeshop.presentation.ui.activity.SupportActivity;
import fpo.thanh.coffeeshop.presentation.ui.adapter.ListThucDonAdapter;
import fpo.thanh.coffeeshop.presentation.ui.listeners.RefreshListTableCallback;

public class ListThucDocFragment extends Fragment {
    @BindView(R.id.rcv_listThucDon)
    RecyclerView rcv_listThucDon;
    @BindView(R.id.fab_cart)
    FloatingActionButton fab_cart;
    @BindView(R.id.tv_badgeCount)
    TextView tv_badgeCount;
    SupportActivity activity;
    List<ThucDon> listData=new ArrayList<>();
    ListThucDonAdapter adapter;
    HashMap<Integer,Integer> hashDsSanPham;
    int maBan;
    int maTang;
    int addSign=0;
    String maHoaDonLocal="";
    private static String tenBan;
    private CountBadge countBadge=new CountBadge();
    private static ListThucDocFragment fragment=null;
    public static ListThucDocFragment getInstance(int maBan, int maTang, String tenBan,int addSign,String maHoaDonLocal){
        fragment=new ListThucDocFragment();
        Bundle bundle=new Bundle();
        bundle.putString("maHoaDonLocal",maHoaDonLocal);
        bundle.putInt("maBan",maBan);
        bundle.putInt("maTang",maTang);
        bundle.putString("tenBan",tenBan);
        bundle.putInt("addSign",addSign);
        fragment.setArguments(bundle);
        return fragment;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        maBan=getArguments().getInt("maBan");
        maTang=getArguments().getInt("maTang");
        tenBan=getArguments().getString("tenBan");
        addSign=getArguments().getInt("addSign");
        maHoaDonLocal=getArguments().getString("maHoaDonLocal");
        Log.e("addSign",""+addSign+" "+maHoaDonLocal);
        adapter=new ListThucDonAdapter(activity,this,maBan,maTang,listData);
        hashDsSanPham=new HashMap<>();
       // countBadge=ViewModelProviders.of(activity).get(new CountBadge().getClass());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_listthucdon,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        fab_cart.setOnClickListener(v -> {
                    activity.getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.layout_holder,new ListThucDonOnTableFragment(hashDsSanPham,this,maBan,tenBan,addSign,maHoaDonLocal))
                            .addToBackStack("holder")
                            .commit();
                });
//        countBadge.getCountBadge().observe(activity, new Observer<Integer>() {
//            @Override
//            public void onChanged(Integer integer) {
//                if(integer<=0){
//                    tv_badgeCount.setVisibility(View.GONE);
//                }else {
//                    tv_badgeCount.setText(String.valueOf(integer));
//                    tv_badgeCount.setVisibility(View.VISIBLE);
//
//                }
//            }
//        });
        setUpRcv();
        updateBadge();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity=(SupportActivity)context;
    }
    private void setUpRcv(){
        listData.clear();
        listData.addAll(activity.mDB.thucDonDAO().getAllThucDon());
        rcv_listThucDon.setLayoutManager(new LinearLayoutManager(activity));
        rcv_listThucDon.setAdapter(adapter);
    }
    public void getDsByLoaiThucDon(String loaiThucDon){
        listData.clear();
        listData.addAll(activity.mDB.thucDonDAO().getThucDonByLoai(loaiThucDon));
        adapter.notifyDataSetChanged();
    }
    public void getAllThucDon(){
        listData.clear();
        listData.addAll(activity.mDB.thucDonDAO().getAllThucDon());
        adapter.notifyDataSetChanged();
    }

    public void sapXepGiaThapDenCao(){
        listData.clear();
        listData.addAll(activity.mDB.thucDonDAO().giaThapDenCao());
        adapter.notifyDataSetChanged();
    }
    public void sapXepGiaCaoDenThap(){
        listData.clear();
        listData.addAll(activity.mDB.thucDonDAO().giaCaoDenThap());
        adapter.notifyDataSetChanged();
    }
    public void updateValueHashDsSanPham(Integer maThucDon,Integer soLuong){
        if(soLuong==0) tv_badgeCount.setVisibility(View.GONE);
        else {
            int slSp = 0;
            if (hashDsSanPham.get(maThucDon) != null) slSp = hashDsSanPham.get(maThucDon);
            hashDsSanPham.put(maThucDon, slSp + soLuong);
            updateBadge();
        }
    }
    public void updateValueHashFromDonHang(Integer maThucDon,Integer soLuong){
        if(soLuong==0) tv_badgeCount.setVisibility(View.GONE);
        else {
            hashDsSanPham.put(maThucDon,soLuong);
            updateBadge();
        }

    }
    private void readHash(){
        for(int i:hashDsSanPham.keySet()){
            Log.e(""+i,hashDsSanPham.get(i)+"");
        }
    }
    private void updateBadge(){
        if(hashDsSanPham.size()==0) tv_badgeCount.setVisibility(View.GONE);
        else tv_badgeCount.setVisibility(View.VISIBLE);
        int sl=0;
        for(int i:hashDsSanPham.keySet()){
            sl+=hashDsSanPham.get(i);
        }
        tv_badgeCount.setText(String.valueOf(sl));
    }
}
