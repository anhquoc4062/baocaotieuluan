package fpo.thanh.coffeeshop.presentation.model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CountBadge extends ViewModel {
    public CountBadge(){
        this.countBadge.setValue(0);
    }

    public MutableLiveData<Integer> getCountBadge() {
        return countBadge;
    }

    public void setCountBadge(MutableLiveData<Integer> countBadge) {
        this.countBadge = countBadge;
    }

    MutableLiveData<Integer> countBadge=new MutableLiveData<>();

}
