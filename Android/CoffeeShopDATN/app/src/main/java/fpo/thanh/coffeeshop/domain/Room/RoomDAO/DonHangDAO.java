package fpo.thanh.coffeeshop.domain.Room.RoomDAO;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import fpo.thanh.coffeeshop.domain.Room.RoomModel.DonHang;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.HoaDon;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface DonHangDAO {
    @Insert(onConflict = REPLACE)
    public void insertDonHang(DonHang donHang);
    @Query("SELECT COUNT(*) FROM DonHang")
    public int getSizeDonHang();
    @Query("DELETE FROM DonHang")
    public void deleteAllDonHang();
    @Query("SELECT * FROM DonHang")
    public  void getListDonHang();
    @Query("SELECT * FROM DonHang where trangThai=:trangThai")
    public void getListDonHangByTrangThai(String trangThai);
    @Query("SELECT * FROM DONHANG where maDonHang=:maDonHang")
    public void getListDonHangByMaDonHang(String maDonHang);
}
