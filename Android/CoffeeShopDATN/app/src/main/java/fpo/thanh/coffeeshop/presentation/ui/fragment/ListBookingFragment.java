package fpo.thanh.coffeeshop.presentation.ui.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import fpo.thanh.coffeeshop.R;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.HoaDon;
import fpo.thanh.coffeeshop.presentation.ui.activity.MainActivity;
import fpo.thanh.coffeeshop.presentation.ui.adapter.ListHoaDonPedingAdapter;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class ListBookingFragment extends Fragment {
    static List<HoaDon> listData;
    static ListHoaDonPedingAdapter adapter=null;
    static MainActivity activity;
    static List<HoaDon> listDataTmp; // this list to control
    @BindView(R.id.rcv_listHoaDon)
    RecyclerView rcv_listHoaDon;
    @BindView(R.id.tv_holderBooking)
    TextView tv_holderBooking;
    @BindView(R.id.swipteRefresh)
    SwipeRefreshLayout swipteRefresh;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_booking,container,false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listData=new ArrayList<>();
        listDataTmp=new ArrayList<>();
        adapter=new ListHoaDonPedingAdapter(activity,listData);
        listData.addAll(activity.mDB.hoaDonDAO().getHoaDonLocal());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        if(listData.size()==0) tv_holderBooking.setVisibility(View.VISIBLE);
        else tv_holderBooking.setVisibility(View.GONE);
        setupRecyclerView();
        swipteRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateList();
                swipteRefresh.setRefreshing(false);
            }
        });
        //autoUpdate();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(adapter!=null)
            updateList();
    }
    private void setupRecyclerView(){
        rcv_listHoaDon.setAdapter(adapter);
        rcv_listHoaDon.setLayoutManager(new LinearLayoutManager(activity));
        adapter.notifyDataSetChanged();
    }
    private void updateList(){
        listDataTmp=activity.mDB.hoaDonDAO().getHoaDonLocal();
        if(listData.size()!=listDataTmp.size()){
            Collections.reverse(listDataTmp);
            listData.clear();
            listData.addAll(listDataTmp);
            adapter.notifyDataSetChanged();
        }
        if(listData.size()==0) tv_holderBooking.setVisibility(View.VISIBLE);
        else tv_holderBooking.setVisibility(View.GONE);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity=(MainActivity)context;
    }
    void autoUpdate(){
        Observable.just(1)
                //.debounce(3000,TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .repeatWhen ((completed)->completed.delay(5, TimeUnit.SECONDS))
                .subscribe(
                        next -> {
                            try {
                                if(adapter!=null){
                                    listDataTmp=activity.mDB.hoaDonDAO().getHoaDonLocal();
                                    if(listData.size()!=listDataTmp.size()) {
                                        activity.runOnUiThread(new Runnable() {

                                            @Override
                                            public void run() {
                                                //Log.e("update cooked nè","OK update nè");
                                                Collections.reverse(listDataTmp);
                                                listData.clear();
                                                listData.addAll(listDataTmp);
                                                adapter.notifyItemMoved(0,0);
                                                //rcv_listHoaDon.smoothScrollToPosition(0);

                                            }
                                        });
                                    }
                                }
                            }
                            catch (Exception e){

                            }
                            //Log.e(listHoaDonPending.get(1).getMaHoaDonLocal(),convertJsonFromListDsMon(mDB.dsMonDAO().getDsMonByMaHoadonLocal(listHoaDonPending.get(1).getMaHoaDonLocal())));
                            // if()

                        },
                        error -> error.printStackTrace(),
                        () -> Log.e("complete","YUP, has been completed")
                );
    }
    public static class RefreshBookingList extends AsyncTask<String,String,String>{

        @Override
        protected String doInBackground(String... strings) {
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            listDataTmp=activity.mDB.hoaDonDAO().getHoaDonLocal();
                Collections.reverse(listDataTmp);
                listData.clear();
                listData.addAll(listDataTmp);
                adapter.notifyDataSetChanged();
        }
    }
}
