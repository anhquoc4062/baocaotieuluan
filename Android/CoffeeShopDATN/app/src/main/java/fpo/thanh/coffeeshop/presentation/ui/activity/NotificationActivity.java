package fpo.thanh.coffeeshop.presentation.ui.activity;

import android.app.Notification;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fpo.thanh.coffeeshop.R;
import fpo.thanh.coffeeshop.domain.Room.AppDatabase;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.NotificationModel;
import fpo.thanh.coffeeshop.presentation.ui.adapter.NotificationAdapter;

public class NotificationActivity extends AppCompatActivity {
    @BindView(R.id.tv_holderNotifnication)
    TextView tv_holderNotifnication;
    @BindView(R.id.rcv_notification)
    RecyclerView rcv_notification;
    NotificationAdapter adapter;
    @BindView(R.id.toolbarNotificationActivity)
    Toolbar toolbar;
    @BindView(R.id.srl_notification)
    SwipeRefreshLayout srl_notification;
    List<NotificationModel> listData=new ArrayList<>();
    AppDatabase mDb;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        mDb=AppDatabase.getDatabase(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
//        NotificationModel model=new NotificationModel();
//        model.setContent("This is notification");
//        model.setHasRead(0);
//        model.setTimeGenerate(getCurrentTime());
//        mDb.notificationDao().insertNotification(model);

        adapter=new NotificationAdapter(this,listData);
        rcv_notification.setAdapter(adapter);
        rcv_notification.setLayoutManager(new LinearLayoutManager(this));
        updateList();
        srl_notification.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateList();
                srl_notification.setRefreshing(false);
            }
        });
    }

    private void updateList() {
        listData.clear();
        listData.addAll(mDb.notificationDao().getAllNotification());
        adapter.notifyDataSetChanged();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
