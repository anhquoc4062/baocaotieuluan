package fpo.thanh.coffeeshop.domain.repository;

import fpo.thanh.coffeeshop.shareModel.ResultModel;
import fpo.thanh.coffeeshop.storage.service.GetAllDataService;

public interface GetAllDataRepository {
    ResultModel getData();
}
