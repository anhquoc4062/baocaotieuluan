package fpo.thanh.coffeeshop.domain.Room.RoomModel;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import fpo.thanh.coffeeshop.shareModel.DsMonModel;

@Entity
public class HoaDon {
    @ColumnInfo(name = "maHoaDon")
    public Integer maHoaDon;
    @ColumnInfo(name = "thoiGianLap")
    public String thoiGianLap;
    @ColumnInfo(name = "maNhanVien")
    public Integer maNhanVien;

    public Integer getMaHoaDon() {
        return maHoaDon;
    }

    public void setMaHoaDon(Integer maHoaDon) {
        this.maHoaDon = maHoaDon;
    }

    public String getThoiGianLap() {
        return thoiGianLap;
    }

    public void setThoiGianLap(String thoiGianLap) {
        this.thoiGianLap = thoiGianLap;
    }

    public Integer getMaNhanVien() {
        return maNhanVien;
    }

    public void setMaNhanVien(Integer maNhanVien) {
        this.maNhanVien = maNhanVien;
    }

    public Integer getMaBan() {
        return maBan;
    }

    public void setMaBan(Integer maBan) {
        this.maBan = maBan;
    }

    public Double getTongTien() {
        return tongTien;
    }

    public void setTongTien(Double tongTien) {
        this.tongTien = tongTien;
    }

    public String getMaHoaDonLocal() {
        return maHoaDonLocal;
    }

    public void setMaHoaDonLocal(String maHoaDonLocal) {
        this.maHoaDonLocal = maHoaDonLocal;
    }

    public Integer getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Integer trangThai) {
        this.trangThai = trangThai;
    }

    public Integer getGiamGia() {
        return giamGia;
    }

    public void setGiamGia(Integer giamGia) {
        this.giamGia = giamGia;
    }

    public Double getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(Double thanhTien) {
        this.thanhTien = thanhTien;
    }

    public String getMaThuNgan() {
        return maThuNgan;
    }

    public void setMaThuNgan(String maThuNgan) {
        this.maThuNgan = maThuNgan;
    }

    @ColumnInfo(name = "maBan")
    public Integer maBan;
    @ColumnInfo(name = "tongTien")
    public Double tongTien;
    @NonNull
    @PrimaryKey(autoGenerate = false)
    public String maHoaDonLocal;
    @ColumnInfo(name = "trangThai")
    public Integer trangThai;
    @ColumnInfo(name = "giamGia")
    public Integer giamGia;
    @ColumnInfo(name = "thanhTien")
    public Double thanhTien;
    @ColumnInfo(name = "maThuNgan")
    public String maThuNgan;

    public String getTenNhanVienThuNgan() {
        return tenNhanVienThuNgan;
    }

    public void setTenNhanVienThuNgan(String tenNhanVienThuNgan) {
        this.tenNhanVienThuNgan = tenNhanVienThuNgan;
    }

    public String getTenNhanVienOrder() {
        return tenNhanVienOrder;
    }

    public void setTenNhanVienOrder(String tenNhanVienOrder) {
        this.tenNhanVienOrder = tenNhanVienOrder;
    }

    @ColumnInfo (name = "tenNhanVienThuNgan")
    public String tenNhanVienThuNgan;
    @ColumnInfo (name = "tenNhanVienOrder")
    public String tenNhanVienOrder;
    public Integer getHasSync() {
        return hasSync;
    }

    public void setHasSync(Integer hasSync) {
        this.hasSync = hasSync;
    }

    @ColumnInfo(name = "hasSync")
    public Integer hasSync;
    public String getTenBan() {
        return tenBan;
    }

    public void setTenBan(String tenBan) {
        this.tenBan = tenBan;
    }

    @ColumnInfo(name = "tenBan")
    public String tenBan;

}
