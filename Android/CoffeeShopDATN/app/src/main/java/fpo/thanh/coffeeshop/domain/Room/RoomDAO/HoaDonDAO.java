package fpo.thanh.coffeeshop.domain.Room.RoomDAO;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import fpo.thanh.coffeeshop.domain.Room.RoomModel.HoaDon;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.Tang;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface HoaDonDAO {
    @Insert(onConflict = REPLACE)
    public void insertHoaDon(HoaDon hoaDon);

    @Query("SELECT COUNT(*) FROM HoaDon")
    public int getSizeHoaDon();

    @Query("DELETE FROM HoaDon")
    public void deleteAllHoaDon();

    @Query("select * from hoadon where trangthai=1")
    public List<HoaDon> getListHoaDonNew();

    @Query("select * from hoadon where trangThai=7 or trangThai=8")
    public  List<HoaDon> getListHoaDonComplete();

    @Query("select * from hoadon where trangThai<=7")
    public List<HoaDon> getListHoaDonTableUsing();

    @Query("select * from hoadon where maHoaDonLocal=:maHoaDonLocal")
    public HoaDon getHoaDonByMaHoaDonLocal(String maHoaDonLocal);

    @Query("select * from hoadon where hasSync=0")
    public List<HoaDon> getHoaDonLocal();

    @Query("select * from hoadon where hasSync=1")
    public List<HoaDon> getHoaDonSynced();
}
