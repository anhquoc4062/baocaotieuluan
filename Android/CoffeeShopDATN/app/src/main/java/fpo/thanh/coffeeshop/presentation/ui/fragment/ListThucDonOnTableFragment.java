package fpo.thanh.coffeeshop.presentation.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Delayed;

import butterknife.BindView;
import butterknife.ButterKnife;
import fpo.thanh.coffeeshop.R;
import fpo.thanh.coffeeshop.domain.Room.AppDatabase;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.DsMon;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.HoaDon;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.ThucDon;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.ThucDonDaDat;
import fpo.thanh.coffeeshop.presentation.ui.activity.SupportActivity;
import fpo.thanh.coffeeshop.presentation.ui.adapter.ListThucDonOnTableAdapter;
import fpo.thanh.coffeeshop.presentation.ui.listeners.RefreshListTableCallback;
import fpo.thanh.coffeeshop.presentation.ui.progressdialog.CustomProgressDialog;
import fpo.thanh.coffeeshop.shareModel.PostChiTietModel;
import fpo.thanh.coffeeshop.storage.savePreference.CacheClient;
import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ListThucDonOnTableFragment extends Fragment {
    @BindView(R.id.rcv_listThucDonOnTable)
    RecyclerView rcv_listThucDonOnTable;
    @BindView(R.id.tv_tongTien)
    TextView tv_tongTien;
    @BindView(R.id.tv_donHangTamTinh)
    TextView tv_donHangTamTinh;
    @BindView(R.id.btnDatDon)
    Button btnDatDon;
    @BindView(R.id.tv_themMon)
    TextView tv_themMon;
    @BindView(R.id.edt_giamGia)
    EditText edt_giamGia;
    @BindView(R.id.btn_apDung)
    Button btn_apDung;
    private ListThucDonOnTableAdapter adapter;
    private HashMap<Integer, Integer> hashThucDon;
    private ListThucDocFragment fragmentListThucDocFragment;
    private int maBan;
    private String tenBan;
    private final int addSign;
    private final String maHoaDonLocal;
    private HoaDon hoaDon;
    private int gia=0;
    private AppDatabase mDB;
    SupportActivity activity;
    List<ThucDon> listThucDon=new ArrayList<>();
    List<Integer> listSoLuong=new ArrayList<>();
    //private ThucDonDaDat thucDonDaDat;
    DsMon dsMonDaDat;
    private PostChiTietModel postChiTietModel;
    private Random rand=new Random();
    public ListThucDonOnTableFragment(HashMap<Integer,Integer> hashThucDon, ListThucDocFragment fragment, int maBan, String tenBan,int addSign,String maHoaDonLocal){
        this.hashThucDon = hashThucDon;
        this.fragmentListThucDocFragment = fragment;
        this.maBan = maBan;
        this.tenBan = tenBan;
        this.addSign = addSign;
        this.maHoaDonLocal = maHoaDonLocal;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        for(int i:hashThucDon.keySet()){
            if(hashThucDon.get(i)>0) { //chir layas nhung san pham co so luong lon hon 0
                listThucDon.add(activity.mDB.thucDonDAO().getThucDonByMaThucDon(i));
                listSoLuong.add(hashThucDon.get(i));
                Log.e("addSign onTable",addSign+" "+maHoaDonLocal);
            }
        }
        //activity.mDB=AppDatabase.getDatabase(activity);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_listthucdon_ontable,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        for(int i=0;i<listSoLuong.size();++i){
            Log.e(listThucDon.get(i).tenThucDon," - "+listSoLuong.get(i));
        }

        tv_themMon.setOnClickListener(v->activity.getSupportFragmentManager().popBackStack());
        adapter=new ListThucDonOnTableAdapter(activity,this,fragmentListThucDocFragment,listThucDon,listSoLuong);
        adapter.notifyDataSetChanged();
        setupRecyclerView();
        btnDatDon.setOnClickListener(v->{
            if(listThucDon.size()==0){
                Toast.makeText(activity, "Hiện tại bạn chưa chọn món", Toast.LENGTH_SHORT).show();
            }else {
                CustomProgressDialog customProgressDialog = new CustomProgressDialog(activity);
                customProgressDialog.setCancelable(false);
                customProgressDialog.show();
                Log.e("chạy", "đang chạy 1");

                new Thread(new Runnable() { //thằng này chạy ngầm, nào nó xong xuôi r nó mưới tắt progress
                    @Override
                    public void run() {
                        if(addSign!=1) {
                            hoaDon = new HoaDon();
                            hoaDon.setMaBan(maBan);//ý đồ là sẽ chạy ngầm add data rồi xong nào load xong thì tắt hết, back activity luôn
                            //hoaDon.setMaHoaDon();
                            double tongTien = (double) gia; // Toongr tienef
                            // Log.e("gias hien tai",gia+"");
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date date = new Date(System.currentTimeMillis());
                            String thoiGianDat = String.valueOf(formatter.format(date));
                            Log.e("thoiwf gian dat", thoiGianDat);
                            long time = new Date().getTime();
                            //Log.e("time",time+"");
                            String maHoaDonLocal = "" + time + "" + (rand.nextInt((99999 - 10000) + 1) + 10000);
                            // Log.e("maHoaDonLocal",maHoaDonLocal);
                            String maChiTietHoaDonLocal;
                            hoaDon.setMaHoaDonLocal(maHoaDonLocal);
                            Log.e("Mahoadonlocal", maHoaDonLocal);
                            hoaDon.setMaHoaDon(0);
                            hoaDon.setGiamGia(0);
                            hoaDon.setThoiGianLap(thoiGianDat);
                            hoaDon.setMaNhanVien(Integer.parseInt(CacheClient.getCache(activity, "maNhanVien")));
                            hoaDon.setMaThuNgan("ThanhNguyen");
                            hoaDon.setTenNhanVienOrder(CacheClient.getCache(activity, "username"));
                            hoaDon.setThanhTien(tongTien);
                            hoaDon.setTrangThai(1);
                            hoaDon.setTongTien(tongTien);
                            hoaDon.setTenBan(tenBan);
                            //Log.e("tên bàn",tenBan);
                            hoaDon.setHasSync(0);
                            String jsonDsMon = "[";
                            Gson gson = new Gson();
                            for (int i = 0; i < listSoLuong.size(); ++i) {
                                //thucDonDaDat=new ThucDonDaDat();
                                //thucDonDaDat
                                maChiTietHoaDonLocal = (time) + "" + (rand.nextInt((99999 - 10000) + 1) + 10000);
                                // Log.e("maChiTietLoca",maChiTietHoaDonLocal);
                                dsMonDaDat = new DsMon();
                                dsMonDaDat.setMaChiTietLocal(maChiTietHoaDonLocal);
                                dsMonDaDat.setGiaMon(listThucDon.get(i).gia);
                                dsMonDaDat.setDonGia(listThucDon.get(i).gia * listSoLuong.get(i));
                                dsMonDaDat.setMaHoaDonLocal(maHoaDonLocal);
                                dsMonDaDat.setMaHoaDon(0);
                                Log.e(listThucDon.get(i).tenThucDon + " " + listSoLuong.get(i), maHoaDonLocal);
                                dsMonDaDat.setMaChiTiet((int) (time % 1000000) + (rand.nextInt((99999 - 10000) + 1) + 10000));
                                dsMonDaDat.setMaThucDon(listThucDon.get(i).maThucDon);
                                dsMonDaDat.setSoLuong(listSoLuong.get(i));
                                dsMonDaDat.setTrangThai(1);
                                dsMonDaDat.setTenThucDon(listThucDon.get(i).tenThucDon);
                                jsonDsMon += gson.toJson(new PostChiTietModel(dsMonDaDat.maChiTiet, dsMonDaDat.maHoaDon,
                                        dsMonDaDat.maHoaDonLocal, dsMonDaDat.maThucDon, dsMonDaDat.soLuong, dsMonDaDat.donGia, dsMonDaDat.maChiTietLocal,
                                        dsMonDaDat.trangThai));
//                        jsonDsMon+=gson.toJson(new Object(){
//                            int maChiTiet=0;
//                            String maHoaDon=maHoaDonLocal;
//                        });
                                jsonDsMon += ",";
                                mDB.dsMonDAO().insertDsMon(dsMonDaDat);
                            }
                            mDB.hoaDonDAO().insertHoaDon(hoaDon);
                            jsonDsMon += "]";
                            // Log.e("json", jsonDsMon);
                            //mDB.hoaDonDAO().getSizeHoaDon();
                            //Log.e("size sau khi inssert", "" + mDB.hoaDonDAO().getSizeHoaDon());
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    //Log.e("Run ui", "OK run");
                                    listSoLuong.clear();
                                    listThucDon.clear();
                                    hashThucDon.clear();
                                    fragmentListThucDocFragment.updateValueHashFromDonHang(0, 0);
                                    customProgressDialog.dismiss();
                                    Toast.makeText(activity, activity.getResources().getString(R.string.camonquykhach), Toast.LENGTH_SHORT).show();
                                    //activity.getSupportFragmentManager().popBackStack();
                                    activity.finish();
                                }
                            });
                        }
                        else {
                            long time = new Date().getTime();
                            HoaDon hoaDon=activity.mDB.hoaDonDAO().getHoaDonByMaHoaDonLocal(maHoaDonLocal);
                            hoaDon.setHasSync(0); // Set chwa sync de tiep tuc sync
                            hoaDon.setTrangThai(2); // da cap nhat hoa don
                            double tongTien = (double) gia; // Toongr tienef
                            for (int i = 0; i < listSoLuong.size(); ++i) {
                                //thucDonDaDat=new ThucDonDaDat();
                                //thucDonDaDat
                                String maChiTietHoaDonLocal="";
                                maChiTietHoaDonLocal = (time) + "" + (rand.nextInt((99999 - 10000) + 1) + 10000);
                                // Log.e("maChiTietLoca",maChiTietHoaDonLocal);
                                dsMonDaDat = new DsMon();
                                dsMonDaDat.setMaChiTietLocal(maChiTietHoaDonLocal);
                                dsMonDaDat.setGiaMon(listThucDon.get(i).gia);
                                dsMonDaDat.setDonGia(listThucDon.get(i).gia * listSoLuong.get(i));
                                dsMonDaDat.setMaHoaDonLocal(hoaDon.getMaHoaDonLocal());
                                dsMonDaDat.setMaHoaDon(hoaDon.getMaHoaDon());
                                Log.e(listThucDon.get(i).tenThucDon + " " + listSoLuong.get(i), hoaDon.getMaHoaDonLocal());
                                dsMonDaDat.setMaChiTiet((int) (time % 1000000) + (rand.nextInt((99999 - 10000) + 1) + 10000));
                                dsMonDaDat.setMaThucDon(listThucDon.get(i).maThucDon);
                                dsMonDaDat.setSoLuong(listSoLuong.get(i));
                                dsMonDaDat.setTrangThai(1);
                                dsMonDaDat.setTenThucDon(listThucDon.get(i).tenThucDon);
                                mDB.dsMonDAO().insertDsMon(dsMonDaDat);
                            }
                            hoaDon.setTongTien(hoaDon.getTongTien()+tongTien);
                            hoaDon.setThanhTien(hoaDon.getThanhTien()+tongTien);
                            mDB.hoaDonDAO().insertHoaDon(hoaDon);
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    //Log.e("Run ui", "OK run");
                                    listSoLuong.clear();
                                    listThucDon.clear();
                                    hashThucDon.clear();
                                    //fragmentListThucDocFragment.updateValueHashFromDonHang(0, 0);
                                    customProgressDialog.dismiss();
                                    Toast.makeText(activity, activity.getResources().getString(R.string.camonquykhach), Toast.LENGTH_SHORT).show();
                                    //activity.getSupportFragmentManager().popBackStack();
                                    activity.finish();
                                }
                            });
                        }//end Else
                    }
                }).start();
            }

            //Log.e("chạy","đang chạy 2");
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity=(SupportActivity)context;
        mDB=activity.mDB;

    }
    private void setupRecyclerView(){
        rcv_listThucDonOnTable.setAdapter(adapter);
        rcv_listThucDonOnTable.setLayoutManager(new LinearLayoutManager(activity));
        adapter.notifyDataSetChanged();
        updateTongGia(listThucDon,listSoLuong);
    }
    public void updateTongGia(List<ThucDon> listThucDon,List<Integer> soLuong){
        gia=0;
        for (int i=0;i<listThucDon.size();++i){
            gia+=(listThucDon.get(i).gia*soLuong.get(i));
        }
        tv_tongTien.setText(String.valueOf(gia*1.0)+"$");
        tv_donHangTamTinh.setText(String.valueOf(gia*1.0)+"$");

    }


}
