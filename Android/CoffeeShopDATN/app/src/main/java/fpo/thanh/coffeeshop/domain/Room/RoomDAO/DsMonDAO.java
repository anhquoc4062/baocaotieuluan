package fpo.thanh.coffeeshop.domain.Room.RoomDAO;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import fpo.thanh.coffeeshop.domain.Room.RoomModel.DsMon;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.HoaDon;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface DsMonDAO {
    @Insert(onConflict = REPLACE)
    public void insertDsMon(DsMon dsmon);
    @Query("SELECT COUNT(*) FROM DsMon")
    public int getSizeDsMon();
    @Query("DELETE FROM DsMon")
    public void deleteAllDsMon();
    @Query("SELECT * FROM DSMON WHERE DSMON.maHoaDonLocal=:maHoaDonLocal")
    public List<DsMon> getDsMonByMaHoadonLocal(String maHoaDonLocal);
}
