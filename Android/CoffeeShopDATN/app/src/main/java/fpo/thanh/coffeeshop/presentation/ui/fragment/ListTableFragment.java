package fpo.thanh.coffeeshop.presentation.ui.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.messaging.FirebaseMessagingService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fpo.thanh.coffeeshop.R;
import fpo.thanh.coffeeshop.domain.Room.AppDatabase;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.BanAn;
import fpo.thanh.coffeeshop.domain.Room.RoomModel.HoaDon;
import fpo.thanh.coffeeshop.presentation.ui.Dialog.ListTangDialog;
import fpo.thanh.coffeeshop.presentation.ui.activity.MainActivity;
import fpo.thanh.coffeeshop.presentation.ui.adapter.ItemOffsetDecoration;
import fpo.thanh.coffeeshop.presentation.ui.adapter.ListTableAdapter;
import fpo.thanh.coffeeshop.presentation.ui.listeners.RefreshListTableCallback;
import fpo.thanh.coffeeshop.presentation.ui.listeners.RefreshListTableCallbackImpl;

public class ListTableFragment extends Fragment implements RefreshListTableCallback.Callback {
    @BindView(R.id.rcv_listTable)
    RecyclerView rcv_listTable;
    @BindView(R.id.swipeRefreshListTable)
    SwipeRefreshLayout swipteRefreshListTable;
    @BindView(R.id.tv_tang)
    TextView tv_tang;
    private RefreshListTableCallbackImpl refreshListTableCallback;
    private static List<BanAn> listData;
    private static ListTableAdapter adapter;
    private GridLayoutManager gridLayoutManager;
    private static AppDatabase mDB;
    private static MainActivity activity;
    private static List<HoaDon> listHoaDon=new ArrayList<>();
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listData=new ArrayList<>();
        refreshListTableCallback=new RefreshListTableCallbackImpl(activity,this);
        adapter=new ListTableAdapter(getActivity(),listData,listHoaDon,refreshListTableCallback);
        adapter.notifyDataSetChanged();
        gridLayoutManager=new GridLayoutManager(getActivity(),2);
        mDB=activity.mDB;


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_listtable,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getActivity(), R.dimen.item_offset);
        rcv_listTable.addItemDecoration(itemDecoration);
        swipteRefreshListTable.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setupRecyclerView();
                swipteRefreshListTable.setRefreshing(false);
            }
        });
        tv_tang.setOnClickListener(v->{

            ListTangDialog listTangDialog=new ListTangDialog(activity,this);
            listTangDialog.show();
        });
        setupRecyclerView();
    }
    void setupRecyclerView(){
        listData.clear();
        listHoaDon.clear();
        listData.addAll(mDB.banAnDAO().getListBanAn());
        listHoaDon.addAll(mDB.hoaDonDAO().getListHoaDonTableUsing());
        rcv_listTable.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity=(MainActivity)context;
    }
    public void getListBanAnByMaTang(int maTang){
        listData.clear();
        listData.addAll(mDB.banAnDAO().getListBanAnByMaTang(maTang));
        adapter.notifyDataSetChanged();
    }
    public void getListBanAn(){
        listData.clear();
        listData.addAll(mDB.banAnDAO().getListBanAn());
        adapter.notifyDataSetChanged();
    }
    public void setTextTang(String tenTang){
        tv_tang.setText(tenTang);
    }

    @Override
    public void onRefreshListTable() {
        setupRecyclerView();
    }

    @Override
    public void onResume() {
        super.onResume();
        //if(mDB.hoaDonDAO().getSizeHoaDon()>listHoaDon.size()){
            listHoaDon.clear();
            listHoaDon.addAll(mDB.hoaDonDAO().getListHoaDonTableUsing());
            adapter.notifyDataSetChanged();
        //}
    }
    public static class RefreshMainActivity extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... strings) {
            return strings[0];
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            listData.clear();
            listHoaDon.clear();
            listData.addAll(mDB.banAnDAO().getListBanAn());
            listHoaDon.addAll(mDB.hoaDonDAO().getListHoaDonTableUsing());
            adapter.notifyDataSetChanged();
            //Log.e("message",s);
            Toast.makeText(activity,s, Toast.LENGTH_SHORT).show();
        }
    }
}
