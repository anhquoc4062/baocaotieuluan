package fpo.thanh.coffeeshop.presentation.ui.Dialog;

import android.content.Context;
import android.opengl.Matrix;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import fpo.thanh.coffeeshop.R;
import fpo.thanh.coffeeshop.presentation.ui.activity.MainActivity;
import fpo.thanh.coffeeshop.presentation.ui.fragment.ListTableFragment;

public class ListTangDialog extends android.app.Dialog implements View.OnClickListener {
    @BindView(R.id.tv_tang5)
    TextView tv_tang5;
    @BindView(R.id.tv_tang6)
    TextView tv_tang6;
    @BindView(R.id.tv_tang7)
    TextView tv_tang7;
    @BindView(R.id.tv_tang8)
    TextView tv_tang8;
    @BindView(R.id.tv_tang9)
    TextView tv_tang9;
    @BindView(R.id.tv_all)
    TextView tv_all;
    ListTableFragment fragment;
    public ListTangDialog(Context context, ListTableFragment fragment){
        super(context);
        this.fragment=fragment;
    }
    public ListTangDialog(@NonNull Context context) {
        super(context);
    }

    public ListTangDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected ListTangDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_listtang);
        ButterKnife.bind(this);
        tv_all.setOnClickListener(this);
        tv_tang5.setOnClickListener(this);
        tv_tang6.setOnClickListener(this);
        tv_tang7.setOnClickListener(this);
        tv_tang8.setOnClickListener(this);
        tv_tang9.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_tang5:
                fragment.getListBanAnByMaTang(5);
                fragment.setTextTang("Tầng trệt");
                this.dismiss();
                break;
            case R.id.tv_tang6:
                fragment.getListBanAnByMaTang(6);
                fragment.setTextTang("Tầng 01");
                this.dismiss();

                break;
            case R.id.tv_tang7:
                fragment.getListBanAnByMaTang(7);
                fragment.setTextTang("Tầng 02");
                this.dismiss();

                break;
            case R.id.tv_tang8:
                fragment.getListBanAnByMaTang(8);
                fragment.setTextTang("Sân vườn");
                this.dismiss();

                break;
            case R.id.tv_tang9:
                fragment.getListBanAnByMaTang(9);
                fragment.setTextTang("Ban công");
                this.dismiss();

                break;
            case R.id.tv_all:
                fragment.getListBanAn();
                fragment.setTextTang("Tất cả");
                this.dismiss();

                break;
                default:break;
        }
    }
}
