package fpo.thanh.coffeeshop.presentation.presenter.impl;

import fpo.thanh.coffeeshop.domain.executor.Executor;
import fpo.thanh.coffeeshop.domain.executor.MainThread;
import fpo.thanh.coffeeshop.domain.interactors.SyncOrderInteractor;
import fpo.thanh.coffeeshop.domain.interactors.impl.SyncOrderInteractorImpl;
import fpo.thanh.coffeeshop.domain.repository.SyncOrderRepository;
import fpo.thanh.coffeeshop.presentation.presenter.AbstractPresenter;
import fpo.thanh.coffeeshop.presentation.presenter.SyncOrderPresenter;
import fpo.thanh.coffeeshop.shareModel.SyncOrderModel;

public class SyncOrderPresenterImpl extends AbstractPresenter implements SyncOrderPresenter, SyncOrderInteractor.Callback {
    private final View view;
    private final SyncOrderRepository repository;

    public SyncOrderPresenterImpl(Executor executor, MainThread mainThread, View view, SyncOrderRepository repository) {
        super(executor, mainThread);
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void onFinish(SyncOrderModel result) {
        view.onShowResultSyncOrder(result);
    }

    @Override
    public void requestSyncOrder(int maHoaDon, String maHoaDonLocal, String maNhanVienOrder, String thoiGianLap, int maBan, String tenBan, String tokenDevice, Double tongTien, int giamGia, Double thanhTien, String dsMonJs,int trangThai) {
        SyncOrderInteractor interactor=new SyncOrderInteractorImpl(mExecutor,mMainThread,this,repository,maHoaDon,maHoaDonLocal,maNhanVienOrder,thoiGianLap,maBan,tenBan,tokenDevice,tongTien,giamGia,thanhTien,dsMonJs,trangThai);
        interactor.execute();
    }
}
