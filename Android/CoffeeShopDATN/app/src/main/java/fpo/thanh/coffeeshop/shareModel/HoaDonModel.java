package fpo.thanh.coffeeshop.shareModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import fpo.thanh.coffeeshop.domain.Room.RoomModel.DsMon;

public class HoaDonModel {
    @SerializedName("tenBan")
    @Expose
    private String tenBan;
    @SerializedName("dsMon")
    @Expose
    private List<DsMonModel> dsMon = null;
    @SerializedName("maHoaDon")
    @Expose
    private Integer maHoaDon;
    @SerializedName("thoiGianLap")
    @Expose
    private String thoiGianLap;
    @SerializedName("maNhanVienOrder")
    @Expose
    private Integer maNhanVienOrder;
    @SerializedName("maBan")
    @Expose
    private Integer maBan;
    @SerializedName("tongTien")
    @Expose
    private Double tongTien;
    @SerializedName("maHoaDonLocal")
    @Expose
    private String maHoaDonLocal;
    @SerializedName("trangThai")
    @Expose
    private Integer trangThai;
    @SerializedName("giamGia")
    @Expose
    private Integer giamGia;
    @SerializedName("thanhTien")
    @Expose
    private Double thanhTien;
    @SerializedName("maThuNgan")
    @Expose
    private String maThuNgan;

    public String getTenNhanVienOrder() {
        return tenNhanVienOrder;
    }

    public void setTenNhanVienOrder(String tenNhanVienOrder) {
        this.tenNhanVienOrder = tenNhanVienOrder;
    }

    public String getTenNhanVienThuNgan() {
        return tenNhanVienThuNgan;
    }

    public void setTenNhanVienThuNgan(String tenNhanVienThuNgan) {
        this.tenNhanVienThuNgan = tenNhanVienThuNgan;
    }

    @SerializedName("tenNhanVienOrder")
    @Expose
    private String tenNhanVienOrder;
    @SerializedName("tenNhanVienThuNgan")
    @Expose
    private String tenNhanVienThuNgan;
    public String getTenBan() {
        return tenBan;
    }

    public void setTenBan(String tenBan) {
        this.tenBan = tenBan;
    }

    public List<DsMonModel> getDsMon() {
        return dsMon;
    }

    public void setDsMon(List<DsMonModel> dsMon) {
        this.dsMon = dsMon;
    }

    public Integer getMaHoaDon() {
        return maHoaDon;
    }

    public void setMaHoaDon(Integer maHoaDon) {
        this.maHoaDon = maHoaDon;
    }

    public String getThoiGianLap() {
        return thoiGianLap;
    }

    public void setThoiGianLap(String thoiGianLap) {
        this.thoiGianLap = thoiGianLap;
    }

    public Integer getMaNhanVienOrder() {
        return maNhanVienOrder;
    }

    public void setMaNhanVienOrder(Integer maNhanVienOrder) {
        this.maNhanVienOrder = maNhanVienOrder;
    }

    public Integer getMaBan() {
        return maBan;
    }

    public void setMaBan(Integer maBan) {
        this.maBan = maBan;
    }

    public Double getTongTien() {
        return tongTien;
    }

    public void setTongTien(Double tongTien) {
        this.tongTien = tongTien;
    }

    public String getMaHoaDonLocal() {
        return maHoaDonLocal;
    }

    public void setMaHoaDonLocal(String maHoaDonLocal) {
        this.maHoaDonLocal = maHoaDonLocal;
    }

    public Integer getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Integer trangThai) {
        this.trangThai = trangThai;
    }

    public Integer getGiamGia() {
        return giamGia;
    }

    public void setGiamGia(Integer giamGia) {
        this.giamGia = giamGia;
    }

    public Double getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(Double thanhTien) {
        this.thanhTien = thanhTien;
    }

    public String getMaThuNgan() {
        return maThuNgan;
    }

    public void setMaThuNgan(String maThuNgan) {
        this.maThuNgan = maThuNgan;
    }

}
