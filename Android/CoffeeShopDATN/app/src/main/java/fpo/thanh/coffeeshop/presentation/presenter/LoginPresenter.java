package fpo.thanh.coffeeshop.presentation.presenter;

import fpo.thanh.coffeeshop.presentation.ui.BaseView;
import fpo.thanh.coffeeshop.shareModel.LoginModel;

public interface LoginPresenter extends BaseView {
    interface View {
        void showResultLogin(LoginModel result);
    }
    void requestLogin(String username,String password);
}
