package fpo.thanh.coffeeshop.domain.interactors.impl;

import fpo.thanh.coffeeshop.domain.executor.Executor;
import fpo.thanh.coffeeshop.domain.executor.MainThread;
import fpo.thanh.coffeeshop.domain.interactors.GetOrderInteractor;
import fpo.thanh.coffeeshop.domain.interactors.base.AbstractInteractor;
import fpo.thanh.coffeeshop.domain.repository.GetOrderRepository;
import fpo.thanh.coffeeshop.domain.repository.Impl.GetOrderRepositoryImpl;
import fpo.thanh.coffeeshop.shareModel.GetOrderModel;

public class GetOrderInteractorImpl extends AbstractInteractor implements GetOrderInteractor {
    private final Callback callback;
    private final String id;
    private final GetOrderRepository repository;

    public GetOrderInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback callback, String id, GetOrderRepository repository) {
        super(threadExecutor, mainThread);
        this.callback = callback;
        this.id = id;
        this.repository = repository;
    }

    @Override
    public void run() {
        GetOrderModel result=repository.getOrder(id);
            mMainThread.post(()->{
                callback.onFinish(result);
            });
    }
}
