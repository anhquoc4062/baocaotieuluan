package fpo.thanh.coffeeshop.storage.service;

import fpo.thanh.coffeeshop.shareModel.SyncOrderModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface SyncOrderService {
    @POST("/api/SyncOrder")
    @FormUrlEncoded
    Call<SyncOrderModel> syncOrder(@Field("maHoaDon") int maHoaDon,@Field("maHoaDonLocal") String maHoaDonLocal,
                                   @Field("maNhanVienOrder")String maNhanVienOrder,@Field("thoiGianLap") String thoiGianLap,
                                   @Field("maBan")int maBan,@Field("tenBan") String tenBan,@Field("tokenThietBi") String tokenThietBi,
                                   @Field("tongTien")Double tongTien,@Field("giamGia") int giamGia,
                                   @Field("thanhTien")Double thanhTien,@Field("dsMonJson") String dsMonJson,@Field("trangThai")int trangThai
                                   );
}
