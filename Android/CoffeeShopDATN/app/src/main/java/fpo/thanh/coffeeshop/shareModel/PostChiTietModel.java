package fpo.thanh.coffeeshop.shareModel;

public class PostChiTietModel {
    int MaChiTiet;
    int MaHoaDon;

    public PostChiTietModel(int maChiTiet, int maHoaDon, String maHoaDonLocal, int maThucDon, int soLuong, Double donGia, String maChiTietLocal, int trangThai) {
        MaChiTiet = maChiTiet;
        MaHoaDon = maHoaDon;
        MaHoaDonLocal = maHoaDonLocal;
        MaThucDon = maThucDon;
        SoLuong = soLuong;
        DonGia = donGia;
        MaChiTietLocal = maChiTietLocal;
        TrangThai = trangThai;
    }

    String MaHoaDonLocal;
    int MaThucDon;
    int SoLuong;
    Double DonGia;
    String MaChiTietLocal;
    int TrangThai;
    public int getMaHoaDon() {
        return MaHoaDon;
    }

    public void setMaHoaDon(int maHoaDon) {
        MaHoaDon = maHoaDon;
    }

    public String getMaHoaDonLocal() {
        return MaHoaDonLocal;
    }

    public void setMaHoaDonLocal(String maHoaDonLocal) {
        MaHoaDonLocal = maHoaDonLocal;
    }

    public int getMaThucDon() {
        return MaThucDon;
    }

    public void setMaThucDon(int maThucDon) {
        MaThucDon = maThucDon;
    }

    public int getSoLuong() {
        return SoLuong;
    }

    public void setSoLuong(int soLuong) {
        SoLuong = soLuong;
    }

    public Double getDonGia() {
        return DonGia;
    }

    public void setDonGia(Double donGia) {
        DonGia = donGia;
    }

    public String getMaChiTietLocal() {
        return MaChiTietLocal;
    }

    public void setMaChiTietLocal(String maChiTietLocal) {
        MaChiTietLocal = maChiTietLocal;
    }

    public int getTrangThai() {
        return TrangThai;
    }

    public void setTrangThai(int trangThai) {
        TrangThai = trangThai;
    }



}
