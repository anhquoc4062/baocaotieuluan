package fpo.thanh.coffeeshop.domain.repository;

import fpo.thanh.coffeeshop.shareModel.GetOrderModel;

public interface GetOrderRepository {
    GetOrderModel getOrder(String id);
}
