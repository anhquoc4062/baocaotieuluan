// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyANIXmVYyXlWz_VO0ATgxZipkEMHJi9-Ac",
    authDomain: "tlecoffeeshop-19101508.firebaseapp.com",
    databaseURL: "https://tlecoffeeshop-19101508.firebaseio.com",
    projectId: "tlecoffeeshop-19101508",
    storageBucket: "tlecoffeeshop-19101508.appspot.com",
    messagingSenderId: "678616141467",
    appId: "1:678616141467:web:187e2c5f4bf626b311803e"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
