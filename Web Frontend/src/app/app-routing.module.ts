import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './directive/main/main.component';
import {AuthenticationComponent} from './directive/authentication/authentication.component';


const routes: Routes = [
  {
    path: 'auth',
    component: AuthenticationComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
