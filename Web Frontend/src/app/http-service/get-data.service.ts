import { ApiUrlHelper } from './../helper/api-helper';
import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { FnParam } from '@angular/compiler/src/output/output_ast';
@Injectable({
  providedIn: 'root'
})
export class GetDataService {

  constructor(
    public http: HttpClient,
    public apiUrlHelper: ApiUrlHelper
  ) { }

  getData() {
    return this.http.get<any>(this.apiUrlHelper.getData, {withCredentials: true});
  }

  getOrderFromServer(id) {
    const param = new FormData();
    param.append('id', id.toString());
    return this.http.post<any>(this.apiUrlHelper.getOrderFromServer, param, { withCredentials: true });

  }
}
