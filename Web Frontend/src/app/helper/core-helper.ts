import {formatDate} from '@angular/common';

export class Helpers {
  static formatDate() {
    let now = new Date();
    return formatDate(now, 'yyyy-MM-dd HH:mm:ss', 'en-US');
  }
}
