import {OrderService} from '../service/order.service';
import {ConstHelper} from './const-helper';
import {HttpClient} from '@angular/common/http'; // add this 1 of 4
import { Injectable, Inject } from '@angular/core';
import {ChiTietHoaDon, HoaDon} from '../model/hoaDon';
import {CurrencyPipe} from '@angular/common';
import {Helpers} from './core-helper';

declare var $: any;

@Injectable()
export class PrintHelper {

  totalDiscount: number = 0;
  totalBeforeDiscount: number = 0;

  constructor(
    // @Inject(TimeAgoPipe) private timeAgo: TimeAgoPipe,
    @Inject(CurrencyPipe) private currencyFormat: CurrencyPipe,
    private orderService: OrderService,
    private constHelper: ConstHelper,
    private http: HttpClient
  ) {
    this.totalDiscount = 0;
  }

  private replaceKey(printFrame, key, value) {
    if (value !== undefined) {
      printFrame.find('body').html(printFrame.find('body').last().html().replace(key, value));
    }
  }

  hideElement(printFrame, key) {
    const closestTrTag = printFrame.find('tr:contains(' + key + '):last').get(0);
    const closestThTag = printFrame.find('th:contains(' + key + '):last').get(0);

    const closestSpanTag = printFrame.find('span:contains(' + key + '):last').get(0);
    const closestDivTag = printFrame.find('div:contains(' + key + '):last').get(0);

    let thisTag = null;

    if (closestThTag !== undefined) { // key on table
      thisTag = closestSpanTag !== undefined ? closestSpanTag : closestDivTag;
    } else {
      if (closestTrTag !== undefined) {
        thisTag = closestTrTag;
      } else {
        thisTag = closestDivTag;
      }
    }
    if (thisTag !== undefined) {
      this.replaceKey(printFrame, thisTag.outerHTML, '');
    }
  }

  removeLastRow(printFrame) {
    const closestTrTag = printFrame.find('tr:contains({{Ten_Mon}}):last').get(0);
    if (closestTrTag != undefined) {
      this.replaceKey(printFrame, closestTrTag.outerHTML, '');
    }
  }

  public async setKeyModel(printFrame, value: any) {
    this.totalDiscount += (value.discount_price != undefined ? +value.discount_price : 0);
    const now = Helpers.formatDate();
    // shop
    this.replaceKey(printFrame, /<\!--.*?-->/g, ''); // remove comment line
    this.replaceKey(printFrame, '{{Ten_Cua_Hang}}', 'TLE Café');
    this.replaceKey(printFrame, '{{So_Dien_Thoai_Cua_Hang}}', '0734567890');
    this.replaceKey(printFrame, '{{Dia_Chi_Cua_Hang}}', '280 An Dương Vương');
    this.replaceKey(printFrame, '{{Logo_Cua_Hang}}', '<img id="logo_shop" style="width: 100px;" src="../../../assets/freeLogo.jpeg"/>');
    $('#logo_shop').attr('src', "../../../assets/freeLogo.jpeg");
    this.replaceKey(printFrame, '{{Cam_On}}', 'Cảm ơn quý khách, hẹn gặp lại');
    // order
    if (value.total != undefined) {
      value.grand_total = value.grand_total != undefined ? value.grand_total : value.total - value.discount_price + value.surcharge_price + value.tax_price;
      if (value.paid_total < value.grand_total) {
        value['own_money'] = value.grand_total - value.paid_total;
        value['change_money'] = 0;
      } else {
        value['change_money'] = value.paid_total - value.grand_total;
        value['own_money'] = 0;
      }
    }
    this.replaceKey(printFrame, '{{Ten_Ban}}', value.tenBan);
    this.replaceKey(printFrame, '{{Ngay_Ban_Hang}}', now);
    this.replaceKey(printFrame, '{{Ten_Phuc_Vu}}', value.tenNhanVienOrder);
    this.replaceKey(printFrame, '{{Ten_Thu_Ngan}}', value.tenThuNgan);
    this.replaceKey(printFrame, '{{So_Khach}}', value.person_quantity);
    // this.hideElement(printFrame, '{{Ten_Ca}}');
    console.log('tong tien', value.tongTien);
    this.replaceKey(printFrame, '{{Tong_Cong}}', value.tongTien != undefined ? '$' + value.tongTien.toFixed(2) : undefined);
    /*if (value.discount_percent <= 0) {
        this.hideElement(printFrame, '{{Giam_Gia}}');
    } else {
        this.replaceKey(printFrame, '{{Giam_Gia}}', value.discount_percent);
    }*/
    this.replaceKey(printFrame, '{{Giam_Gia}}', value.giamGia);
    if (value.discount_price <= 0) {
      this.hideElement(printFrame, '{{Tien_Giam_Gia}}');
    } else {
      this.replaceKey(printFrame, '{{Tien_Giam_Gia}}', '$' + ((value.tongTien * value.giamGia) / 100).toFixed(2));
    }

    this.hideElement(printFrame, '{{Tien_Dat_Coc}}');

    if (value.paid_total <= 0) {
      this.hideElement(printFrame, '{{Tien_Khach_Dua}}');
      this.hideElement(printFrame, '{{Khach_Hang_Dua}}');
    } else {
      this.replaceKey(printFrame, '{{Tien_Khach_Dua}}', this.currencyFormat.transform(value.paid_total));
      this.replaceKey(printFrame, '{{Khach_Hang_Dua}}', this.currencyFormat.transform(value.paid_total));
    }

    if (value['own_money'] <= 0 || value.status == 1) {
      this.hideElement(printFrame, '{{Tien_No}}');
    } else {
      this.replaceKey(printFrame, '{{Tien_No}}', this.currencyFormat.transform(value['own_money']));
    }

    if (value['change_money'] <= 0 || value.status == 1) {
      this.hideElement(printFrame, '{{Tien_Tra}}');
    } else {

      this.replaceKey(printFrame, '{{Tien_Tra}}', this.currencyFormat.transform(value['change_money']));
    }

    this.replaceKey(printFrame, '{{Tien_Mat}}', '$'  + parseFloat(value.thanhTien).toFixed(2));
    // orderItem
    if (value.length != undefined) {

      console.log(value);
      let trString = printFrame.find('tr:contains({{Ten_Mon}})').get(0);
      trString = trString.outerHTML;
      let i = 0;
      await value.forEach((item) => {
        /*if (i != value.length - 1) {
            printFrame.find('tbody:contains({{Ten_Mon}}):last').append(trString);
        }*/
        printFrame.find('tbody:contains({{Ten_Mon}}):last').append(trString);

        this.replaceKey(printFrame, '{{STT}}', i + 1);
        ///////////////////
        this.replaceKey(printFrame, '{{Ngay_Gio_Cap_Nhat_Bep}}', item.updated_time);
        const discountPercent = item['price'] > 0 ? (parseInt(item['discount_price']) * 100) / parseInt(item['price']) : 0;
        item['discount_percent'] = item['discount_percent'] != undefined ? item['discount_percent'] : discountPercent;

        if (item['status'] != this.constHelper.ORDER_STATUS_CANCEL) {
          this.replaceKey(printFrame, '{{Ten_Mon}}', item['tenThucDon']);
        } else {
          this.replaceKey(printFrame, '{{Ten_Mon}}',
            '<span style="text-decoration: line-through">' + item['tenThucDon'] + '</span>');
        }
        this.replaceKey(printFrame, '{{Ten_Mon_Mo_Rong}}', '');
        item['order_item_unit_id'] = item['order_item_unit_id'] == null || item['order_item_unit_id'] == 'null' ? '' : item['order_item_unit_id'];
        this.replaceKey(printFrame, '{{DVT}}', item['order_item_unit_id']);
        this.replaceKey(printFrame, '{{So_Luong}}', +item['soLuong']);
        this.replaceKey(printFrame, '{{Don_Gia}}', '$' + parseFloat(item['giaMon']).toFixed(2));
        this.replaceKey(printFrame, '{{Thanh_Tien}}', '$' + parseFloat(item['donGia']).toFixed(2));

        if (item['discount_percent'] <= 0) {
          this.hideElement(printFrame, '{{Gia_Giam}}');
          this.hideElement(printFrame, '{{CK}}');
        } else {
          this.replaceKey(printFrame, '{{Gia_Giam}}', item['discount_percent']);
          this.replaceKey(printFrame, '{{CK}}', item['discount_percent']);
        }
        this.totalDiscount += +item['discount_price'] ? +item['discount_price'] : 0;
        this.totalBeforeDiscount += (+item['regular_price'] * parseFloat(item['quantity']));


        i = i + 1;
      });

      this.removeLastRow(printFrame);
    }

  }

  async mergeItemOptimize(items) {
    return new Promise(async (resolve) => {
      const mergedItems = {}, arrayReturn = [];
      setTimeout(async () => {
        for await (const item of items) {
          const tmpItem = {...item};
          const compareKey = tmpItem.product_id + '-' + +tmpItem.regular_price + '-' + tmpItem.extra + '-' + tmpItem.status;
          if (mergedItems[compareKey]) {
            mergedItems[compareKey].quantity = +mergedItems[compareKey].quantity + +tmpItem.quantity;
            mergedItems[compareKey].total = +mergedItems[compareKey].total + +tmpItem.total;
            mergedItems[compareKey].grand_total = +mergedItems[compareKey].grand_total + +tmpItem.grand_total;
          } else {
            mergedItems[compareKey] = tmpItem;
          }
          // }
        }
        resolve(Object.keys(mergedItems).map((key) => mergedItems[key]));
      }, 10);
    });

  }

  async setDataPrint(order: HoaDon) {
    return new Promise((resolve, reject) => {
      const printBill = $('#print_bill').contents();
      const promise = new Promise((exe) => {
        this.setKeyModel(printBill, order);
        this.setKeyModel(printBill, order.dsMon);
        resolve();
      });
    });
  }

  printOrder(order: HoaDon) {
    this.totalDiscount = 0;
    this.totalBeforeDiscount = 0;
    return new Promise(async (resolve) => {
      console.log($('#print_bill').get(0));
      const printBill = $('#print_bill').get(0).contentWindow;
      this.http.get('../../assets/print_template.txt', {responseType: 'text'}).subscribe(data => {
        printBill.document.write(data);
        printBill.document.close();
        return this.setDataPrint(order).then(() => {
          setTimeout(() => {
            printBill.print();
          }, 300);
        });
      });

    });
  }
}

