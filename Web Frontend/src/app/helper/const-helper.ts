export class ConstHelper {
  public ORDER_STATUS_NEW = 1;
  public ORDER_STATUS_UPDATE = 2;
  public ORDER_STATUS_CANCEL = 3;
  public ORDER_STATUS_CHANGE = 4;
  public ORDER_STATUS_MERGE = 5; // IGNORE
  public ORDER_STATUS_COOKING = 6;
  public ORDER_STATUS_COOKED = 7;
  public ORDER_STATUS_COMPLETE = 8;

  public ORDER_STATUS_HIDE = [
     3, 4, 5, 8
  ];

  public QUEUE_SIGNAL_NEW = 1;
  public QUEUE_SIGNAL_COMPLETE = 2;
  public QUEUE_SIGNAL_FAIL = 3;
  public QUEUE_SIGNAL_SERVER = 4;
  public QUEUE_SIGNAL_SYNCING = 5;
}
