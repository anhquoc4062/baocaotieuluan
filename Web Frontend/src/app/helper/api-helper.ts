// const mainUrl = 'http://192.168.2.31:45455/';
const mainUrl = 'http://localhost:5000/';
export class ApiUrlHelper {

  getData =  mainUrl + 'api/getdata';
  uploadProduct = mainUrl + 'uploads/product_small/';

  syncOrder = mainUrl + 'api/syncOrder';
  testPost = mainUrl + 'api/testPost';
  getOrderFromServer = mainUrl + 'api/getOrderFromServer';

  login = mainUrl + 'api/login';
  pushToken = mainUrl + 'api/pushToken';
  popToken = mainUrl + 'api/popToken';
}
