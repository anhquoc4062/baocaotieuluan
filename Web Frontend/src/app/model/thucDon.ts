export interface ThucDon {
  tenLoai: string,
  maThucDon: number,
  tenThucDon: string,
  hinhAnh: string,
  maLoai: number,
  gia: number,
  khuyenMai: number,
  moTa: string,
  giaKhuyenMai: number,
}
