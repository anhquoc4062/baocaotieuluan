export interface LoaiThucDon {
  maLoai: number,
  tenLoai: string,
  maLoaiCha: number,
}
