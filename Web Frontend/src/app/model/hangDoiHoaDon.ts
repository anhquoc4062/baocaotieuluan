export interface HangDoiHoaDon {
  maHangDoi: number,
  maHoaDon: string,
  maHoaDonLocal: string,
  ngayLap: string,
  trangThai: number,
  tinHieu: number,
}
