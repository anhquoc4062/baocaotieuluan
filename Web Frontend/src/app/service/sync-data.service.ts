import { CurrentUserService } from './current-user.service';
import { HoaDon } from './../model/hoaDon';
import { Helpers } from './../helper/core-helper';
import { ApiUrlHelper } from './../helper/api-helper';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SyncDataService {

  constructor(
    private http: HttpClient,
    private currentUser: CurrentUserService,
    private apiUrlHelper: ApiUrlHelper
  ) { }

  async syncData(order: HoaDon) {
    const formData = new FormData();

    formData.append('maHoaDon', order.maHoaDonLocal.toString());
    formData.append('maHoaDonLocal', order.maHoaDonLocal.toString());
    formData.append('maNhanVienOrder', order.maNhanVienOrder != null ? order.maNhanVienOrder.toString() : '');
    formData.append('thoiGianLap', order.thoiGianLap.toString());
    formData.append('maBan', order.maBan.toString());
    formData.append('tenBan', order.tenBan.toString());
    formData.append('tongTien', order.tongTien.toString());
    formData.append('giamGia', order.giamGia.toString());
    formData.append('thanhTien', order.thanhTien.toString());
    formData.append('maThuNgan', order.maThuNgan != null ? order.maThuNgan.toString() : '');
    formData.append('trangThai', order.trangThai.toString());
    formData.append('tokenThietBi', this.currentUser.getUserToken());
    const array = [];
    for (const item of order.dsMon) {
      item.maChiTiet = '0';
      item.maHoaDon = '0';
      item.maHoaDonLocal = order.maHoaDonLocal;
      array.push(item);
    }
    formData.append('dsMonJson', JSON.stringify(array));
    return this.http.post<any>(this.apiUrlHelper.syncOrder, formData, { withCredentials: true });
  }
}
