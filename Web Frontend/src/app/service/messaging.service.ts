import { ToastService } from './toast.service';
import { Injectable } from '@angular/core';
import {BehaviorSubject, Subscription} from 'rxjs';
import {AngularFireDatabase} from '@angular/fire/database';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFireMessaging} from '@angular/fire/messaging';
import {take} from 'rxjs/operators';
import { OrderQueueService } from './order-queue.service';

@Injectable({
  providedIn: 'root'
})
export class MessagingService {
  currentMessage = new BehaviorSubject(null);
  subscription: Subscription[] = [];
  constructor(
    private angularFireDB: AngularFireDatabase,
    private angularFireAuth: AngularFireAuth,
    private toast: ToastService,
    private orderQueueService: OrderQueueService,
    private angularFireMessaging: AngularFireMessaging) {
    this.subscription.push(this.angularFireMessaging.messaging.subscribe(
      (messaging) => {
        messaging.onMessage = messaging.onMessage.bind(messaging);
        messaging.onTokenRefresh = messaging.onTokenRefresh.bind(messaging);
      }
    ));
  }

  /**
   * update token in firebase database
   *
   * @param userId userId as a key
   * @param token token as a value
   */
  updateToken(userId, token) {
    // we can change this function to request our backend service
    this.subscription.push(this.angularFireAuth.authState.pipe(take(1)).subscribe(
      () => {
        const data = {};
        data[userId] = token;
        this.angularFireDB.object('fcmTokens/').update(data);
      }));
  }

  /**
   * request permission for notification from firebase cloud messaging
   *
   * @param userId userId
   */
  requestPermission(userId) {
    this.subscription.push(this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        console.log('token', token);
        localStorage.setItem('currentUserToken', token);
        this.updateToken(userId, token);
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    ));
  }

  /**
   * hook method when new notification received in foreground
   */
  receiveMessage() {
    this.subscription.push(this.angularFireMessaging.messages.subscribe(
      (payload) => {
        console.log('new message received. ', payload);
        this.toast.showSuccess(payload['data'].message);
        const data = payload['data'];
        const newQueue = {
          ngayLap: data.created_date,
          maHoaDon: data.order_id,
          maHoaDonLocal: data.order_local_id,
          trangThai: data.status,
          tinHieu: 4,
        };
        this.orderQueueService.addToOrderQueue(newQueue);

        this.currentMessage.next(payload);
      }));
  }

}
