import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CurrentUserService {

  constructor() { }

  getUserToken() {
    return localStorage.getItem('currentUserToken');
  }

  getUser() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  getUserName() {
    return this.getUser().tenTaiKhoan;
  }

  getUserID() {
    return this.getUser().maTaiKhoan;
  }

  getUserAvatar() {
    return this.getUser().anhDaiDien;
  }

  getUserEmail() {
    return this.getUser().email;
  }
}
