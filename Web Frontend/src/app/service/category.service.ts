import {EventEmitter, Injectable, Output} from '@angular/core';
import {IndexdbService} from './indexdb.service';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  isSelected: any = null;
  @Output() listDisplayCategories = new EventEmitter();

  constructor(
    private indexDBService: IndexdbService
  ) { }

  getAllCategories() {
    return this.indexDBService._loaiThucDon.toArray();
  }

  setDisplayCaregories() {
    this.getAllCategories().then(res => {
      this.listDisplayCategories.emit(res);
    });
  }
}
