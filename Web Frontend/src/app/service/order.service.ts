import { CurrentUserService } from 'src/app/service/current-user.service';
import { HangDoiHoaDon } from './../model/hangDoiHoaDon';
import {EventEmitter, Injectable, Output} from '@angular/core';
import {IndexdbService} from './indexdb.service';
import {ChiTietHoaDon, HoaDon} from '../model/hoaDon';
import {ProductService} from './product.service';
import {Helpers} from '../helper/core-helper';
import {TableService} from './table.service';
import {ConstHelper} from '../helper/const-helper';
import { OrderQueueService } from './order-queue.service';
import { BanAn } from '../model/banAn';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  isSelectedOrder: any = null;
  isChangedNotSave: boolean = false;
  cancelableOrder: boolean = false;
  preOrder: HoaDon = null;
  @Output() orderDisplay = new EventEmitter();
  @Output() orderListServing = new EventEmitter();

  //kitchen bar
  @Output() kitchenBar1 = new EventEmitter();
  @Output() kitchenBar2 = new EventEmitter();
  @Output() kitchenBar3 = new EventEmitter();
  @Output() kitchenBar4 = new EventEmitter();
  barHasOrder: any = [ false, false, false, false ];

  isChangingTable = false;
  currentSelectedOrder:HoaDon = null;

  constructor(
    private indexDB: IndexdbService,
    private productSevice: ProductService,
    private tableService: TableService,
    private constHelper: ConstHelper,
    private orderQueueService: OrderQueueService,
    private currentUser: CurrentUserService
  ) { }

  getOrderByTable(tableId) {
    return this.indexDB._hoaDon.where('maBan').equals(tableId)
    .filter(item => !this.constHelper.ORDER_STATUS_HIDE.includes(item.trangThai)).first().then(order => {
      this.preOrder = order;
      this.orderDisplay.emit(this.preOrder);
    });
  }

  generateId() {
    return new Date().getTime().toString() + (Math.floor(Math.random() * 99999) + 10000).toString();
  }

  createOrder() {
    const generateId = this.generateId();
    this.indexDB._banAn.where('maBan').equals(this.tableService.selectedTableId).first().then((table: BanAn) => {
      const newOrder: HoaDon = {
        dsMon: [],
        maHoaDon: generateId,
        maHoaDonLocal: generateId,
        thoiGianLap: Helpers.formatDate(),
        maBan: table.maBan,
        tenBan: table.tenBan + '-' + table.tenTang,
        trangThai: null,
        tongTien: 0,
        thanhTien: 0,
        giamGia: 0,
        maNhanVienOrder: +this.currentUser.getUserID(),
        tenNhanVienOrder: this.currentUser.getUserName(),
        tenThuNgan: '',
        maThuNgan: 0,
        flg_cooked: false
      };
      this.preOrder = newOrder;
      this.orderDisplay.emit(this.preOrder);
    });

  }

  createOrderNotEmit() {
    return new Promise(resolve => {

      const generateId = this.generateId();
      this.indexDB._banAn.where('maBan').equals(this.tableService.selectedTableId).first().then((table: BanAn) => {
        const newOrder: HoaDon = {
          dsMon: [],
          maHoaDon: generateId,
          maHoaDonLocal: generateId,
          thoiGianLap: Helpers.formatDate(),
          maBan: table.maBan,
          tenBan: table.tenBan + '-' + table.tenTang,
          trangThai: null,
          tongTien: 0,
          thanhTien: 0,
          giamGia: 0,
          maNhanVienOrder: +this.currentUser.getUserID(),
          tenNhanVienOrder: this.currentUser.getUserName(),
          tenThuNgan: '',
          maThuNgan: 0,
          flg_cooked: false
        };
        this.preOrder = newOrder;
        resolve();
      });
    });
  }

  addOrder(productId) {
    const generateId = this.generateId();
    this.productSevice.getProductById(productId).then(async product => {
      const newItem: ChiTietHoaDon = {
        tenThucDon: product.tenThucDon,
        giaMon: product.giaKhuyenMai,
        maChiTiet: generateId,
        maHoaDon: this.preOrder.maHoaDon,
        maHoaDonLocal: this.preOrder.maHoaDonLocal,
        maChiTietLocal: generateId,
        maThucDon: product.maThucDon,
        soLuong: 1,
        donGia: product.giaKhuyenMai,
        trangThai: 1
      };
      let checkExisted = false;
      await this.preOrder.dsMon.forEach((item) => {
        if (item.trangThai == 1 && item.maThucDon == newItem.maThucDon) {
          item.soLuong += newItem.soLuong;
          checkExisted = true;
        }
      });
      if (checkExisted == false) {
        this.preOrder.dsMon.push(newItem);
      }
      this.preOrder.tongTien += newItem.giaMon;
      this.preOrder.thanhTien = this.preOrder.tongTien - ((this.preOrder.tongTien * this.preOrder.giamGia) / 100);
    });
  }

  async updateQuantityOrder(itemId, quantity) {
    await this.preOrder.dsMon.forEach(item => {
      if (item.maChiTiet == itemId) {
        const oldTotal = item.giaMon * item.soLuong;
        item.soLuong = quantity;
        this.preOrder.tongTien -= oldTotal;
        this.preOrder.tongTien += item.giaMon * item.soLuong;
        this.preOrder.thanhTien = this.preOrder.tongTien - ((this.preOrder.tongTien * this.preOrder.giamGia) / 100);
      }
    });
  }

  updateDBOrder(order: HoaDon, serverFlag = false) {
    console.log(order);
    return new Promise(resolve => {
      if (order.trangThai == null) {
        order.trangThai = 1;
      } else if (order.trangThai == 1 || (order.flg_cooked != true && order.trangThai == this.constHelper.ORDER_STATUS_COOKED)) {
        order.trangThai = 2;
      }
      if (serverFlag == false) {
        const newQueue = {
          ngayLap: Helpers.formatDate(),
          maHoaDon: order.maHoaDon,
          maHoaDonLocal: order.maHoaDonLocal,
          trangThai: order.trangThai,
          tinHieu: 1,
        };
        this.orderQueueService.addToOrderQueue(newQueue).then(() => {});
      }
      this.indexDB._hoaDon.add(order).then(() => {
        resolve();
      }, () => {

        console.log('order before update', order);
        this.indexDB._hoaDon.update(order.maHoaDon, order).then(() => {
          resolve();
        });
      });
    });
  }

  getOrderByLocalId(localId: string) {
    return this.indexDB._hoaDon.where('maHoaDonLocal').equals(localId).first();
  }

  getOrderServing() {
    return this.indexDB._hoaDon.filter(x => x.trangThai == this.constHelper.ORDER_STATUS_UPDATE || x.trangThai == this.constHelper.ORDER_STATUS_NEW).toArray();
  }
}
