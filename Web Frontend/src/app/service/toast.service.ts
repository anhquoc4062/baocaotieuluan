import { Injectable } from '@angular/core';
import {ToastrService} from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  toastOption = {
    positionClass: 'toast-bottom-right',
    timeOut: 1500
  };

  constructor(
    private toastr: ToastrService
  ) { }

  showSuccess(message) {
    this.toastr.success(null, message, this.toastOption);
  }

  showError(message) {
    this.toastr.error(null, message, this.toastOption);
  }
}
