import { ConstHelper } from './../helper/const-helper';
import { IndexdbService } from 'src/app/service/indexdb.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrderQueueService {

  constructor(
    private indexDBService: IndexdbService,
    private constHelper: ConstHelper
  ) { }

  addToOrderQueue(data) {
    return this.indexDBService._hangDoiHoaDon.add(data);
  }

  getOrderNotSync() {
    return this.indexDBService._hangDoiHoaDon.where('tinHieu').equals(this.constHelper.QUEUE_SIGNAL_NEW).toArray();
  }

  getOrderFromServer() {
    return this.indexDBService._hangDoiHoaDon.where('tinHieu').equals(this.constHelper.QUEUE_SIGNAL_SERVER).toArray();
  }

  updateOrderQueue(data) {
    return this.indexDBService._hangDoiHoaDon.update(data.maHangDoi, data);
  }
}
