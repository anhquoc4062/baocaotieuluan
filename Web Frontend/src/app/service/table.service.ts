import {EventEmitter, Injectable, Output} from '@angular/core';
import {IndexdbService} from './indexdb.service';
import {ConstHelper} from '../helper/const-helper';

@Injectable({
  providedIn: 'root'
})
export class TableService {

  selectedTableId: any = null;

  @Output() listDisplayTables = new EventEmitter();
  @Output() isSelectedTable = new EventEmitter();

  constructor(
    private indexDBService: IndexdbService,
    private constHelper: ConstHelper,
  ) { }

  getAllTables() {
    this.indexDBService._banAn.toArray();
  }

  async getTableByRoom(roomId) {
    let listTable = [];
    if (roomId == 0) {// is open room
      await this.indexDBService._hoaDon.toArray(async (orders) => {
        for await (const order of orders) {
          if (!this.constHelper.ORDER_STATUS_HIDE.includes(order.trangThai)) {
            await this.indexDBService._banAn.where('maBan').equals(order.maBan).first().then((table) => {
                table['has_order'] = true;
                table['class'] = order.trangThai == this.constHelper.ORDER_STATUS_COOKED ? 'cooked' : 'has-order';
                table['ngayLapHoaDon'] = new Date(order.thoiGianLap);
                table['tenNhanVienOrder'] = order.tenNhanVienOrder;
                listTable.push(table);
            });
          }
        }
      });
    } else {
      await this.indexDBService._banAn.where('maTang').equals(roomId).toArray().then(async (tables) => {
        for await (const table of tables) {
          await this.indexDBService._hoaDon.where('maBan').equals(table.maBan).toArray().then((orders) => {
            const order = orders.filter(item => !this.constHelper.ORDER_STATUS_HIDE.includes(item.trangThai))[0];
            // console.log('order', order);
            if (order != undefined) {
                table['has_order'] = true;
                table['class'] = order.trangThai == this.constHelper.ORDER_STATUS_COOKED ? 'cooked' : 'has-order';
                table['ngayLapHoaDon'] = new Date(order.thoiGianLap);
                table['tenNhanVienOrder'] = order.tenNhanVienOrder;
            } else {
              table['has_order'] = false;
            }
            listTable.push(table);
          });
        }
        listTable = tables;
      });
    }
    return listTable;
  }

  getTableById(tableId) {
    return this.indexDBService._banAn.where('maBan').equals(tableId).first();
  }

  setDisplayTablesByRoomId(roomId) {
    this.getTableByRoom(roomId).then(value => {
      this.listDisplayTables.emit(value);
    });
  }
}
