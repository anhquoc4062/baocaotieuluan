import { Injectable } from '@angular/core';
import Dexie from 'dexie';
import {ThucDon} from '../model/thucDon';
import {BanAn} from '../model/banAn';
import {LoaiThucDon} from '../model/loaiThucDon';
import {HoaDon} from '../model/hoaDon';
import {GetDataService} from '../http-service/get-data.service';
import {Tang} from '../model/tang';
import { HangDoiHoaDon } from '../model/hangDoiHoaDon';

@Injectable({
  providedIn: 'root'
})
export class IndexdbService {

  _thucDon: Dexie.Table<ThucDon, string>;
  _banAn: Dexie.Table<BanAn, string>;
  _loaiThucDon: Dexie.Table<LoaiThucDon, string>;
  _tang: Dexie.Table<Tang, string>;
  _hoaDon: Dexie.Table<HoaDon, string>;
  _hangDoiHoaDon: Dexie.Table<HangDoiHoaDon, string>;

  db = new Dexie('TLECoffeeDB');
  constructor(
    private getDataService: GetDataService
  ) {
    this.db.version(1).stores({
      thucDon: '++maThucDon,tenThucDon,maLoai,tenLoai,hinhAnh,gia,khuyenMai,moTa,giaKhuyenMai',
      banAn: '++maBan,tenTang,maBan,tenBan,maTang',
      loaiThucDon: '++maLoai,tenLoai,maLoaiCha',
      tang: '++maTang,tenTang',
      hoaDon: '++maHoaDon,thoiGianLap,maNhanVien,maBan,tongTien,maHoaDonLocal,trangThai,dsMon',
      hangDoiHoaDon: '++maHangDoi,maHoaDon,maHoaDonLocal,ngayLap,trangThai,tinHieu',
    });
    this._thucDon = this.db.table('thucDon');
    this._banAn = this.db.table('banAn');
    this._loaiThucDon = this.db.table('loaiThucDon');
    this._tang = this.db.table('tang');
    this._hoaDon = this.db.table('hoaDon');
    this._hangDoiHoaDon = this.db.table('hangDoiHoaDon');
  }
  getData() {
    this.getDataService.getData().subscribe(result => {
      this.parseDataFromServer(result.data);
    });
  }
  parseDataFromServer(data) {
    console.log('data', data);
    this._thucDon.clear().then(async () => {
      /*for await (const item of data.thucDon) {
        this._thucDon.add(item);
      }*/
      this._thucDon.bulkAdd(data.thucDon);
      console.log('done thucDon');
    });
    this._banAn.clear().then(async () => {
      /*for await (const item of data.banAn) {
        this._banAn.add(item);
      }*/
      this._banAn.bulkAdd(data.banAn);
      console.log('done banAn');
    });

    this._tang.clear().then(async () => {
      /*for await (const item of data.tang) {
        this._tang.add(item);
      }*/
      this._tang.bulkAdd(data.tang);
      console.log('done tang');
    });

    this._loaiThucDon.clear().then(async () => {
      /*for await (const item of data.loaiThucDon) {
        this._loaiThucDon.add(item);
      }*/
      this._loaiThucDon.bulkAdd(data.loaiThucDon);
      console.log('done loaiThucDon');
    });

    this._hoaDon.clear().then(async () => {
      /*for await (const item of data.hoaDon) {
        this._hoaDon.add(item);
      }*/
      this._hoaDon.bulkAdd(data.hoaDon);
      console.log('done hoaDon');
    });

    this._hangDoiHoaDon.clear();
  }
}
