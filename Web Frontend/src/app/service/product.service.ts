import {EventEmitter, Injectable, Output} from '@angular/core';
import {IndexdbService} from './indexdb.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  @Output() listDisplayProducts = new EventEmitter();

  constructor(
    private indexDB: IndexdbService
  ) { }

  getProductByCategoryId(categoryId) {
    return this.indexDB._thucDon.where('maLoai').equals(categoryId).toArray();
  }

  getProductById(id) {
    return this.indexDB._thucDon.where('maThucDon').equals(id).first();
  }

  setDisplayProductsByCategoryId(categoryId) {
    if (categoryId != null) {
      this.getProductByCategoryId(categoryId).then(products => {
        this.listDisplayProducts.emit(products);
      });
    }
  }
}
