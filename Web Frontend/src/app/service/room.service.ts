import {EventEmitter, Injectable, Output} from '@angular/core';
import {IndexdbService} from './indexdb.service';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  isSelected: any = null;
  @Output() listDisplayRooms = new EventEmitter();

  constructor(
    private indexDBService: IndexdbService
  ) { }

  getAllRooms() {
    return this.indexDBService._tang.toArray();
  }

  setDisplayRooms() {
    this.getAllRooms().then(res => {
      this.listDisplayRooms.emit(res);
    });
  }
}
