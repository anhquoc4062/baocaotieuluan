import { FormsModule } from '@angular/forms';
import { LottieAnimationViewModule } from 'ng-lottie';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './directive/main/main.component';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFireMessagingModule} from '@angular/fire/messaging';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import {MessagingService} from './service/messaging.service';
import {AsyncPipe, CurrencyPipe} from '@angular/common';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {GetDataService} from './http-service/get-data.service';
import { ContentComponent } from './directive/main/content/content.component';
import { OrderComponent } from './directive/main/order/order.component';
import { HeaderComponent } from './directive/main/header/header.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule, MatGridListModule, MatIconModule, MatListModule, MatMenuModule, MatToolbarModule} from '@angular/material';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import { MenuComponent } from './directive/main/content/menu/menu.component';
import { CategoryComponent } from './directive/main/content/category/category.component';
import { FeatureComponent } from './directive/main/content/feature/feature.component';
import {Toast, ToastrModule} from 'ngx-toastr';
import {TimeAgoPipe} from 'time-ago-pipe';
import {TimeAgoXPipe} from './helper/time-ago-pipe';
import { AuthenticationComponent } from './directive/authentication/authentication.component';
import {MainModule} from './directive/main/main.module';
import { OwlModule } from 'ngx-owl-carousel';
import {PrintHelper} from './helper/print.helper';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    // ContentComponent,
    // OrderComponent,
    HeaderComponent,
    // MenuComponent,
    // CategoryComponent,
    // FeatureComponent,
    TimeAgoXPipe,
    AuthenticationComponent
  ],
  imports: [
    BrowserModule,
    MainModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),
    HttpClientModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    PerfectScrollbarModule,
    MatIconModule,
    ToastrModule.forRoot(),
    MatButtonModule,
    MatMenuModule,
    LottieAnimationViewModule,
    FormsModule,
    OwlModule
  ],
  providers: [
    MessagingService,
    AsyncPipe,
    HttpClient,
    GetDataService,
    PrintHelper,
    CurrencyPipe
    // AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
