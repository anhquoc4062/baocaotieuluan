import { ApiUrlHelper } from './../../helper/api-helper';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private apiUrlHelper: ApiUrlHelper
  ) { }

  isAuthenticated() {
    const currentUser = localStorage.getItem('currentUser');
    if (currentUser && currentUser != '') {
      return true;
    }
    return false;
  }

  login(username, password) {
    const formData = new FormData();
    formData.append('username', username);
    formData.append('password', password);
    return this.http.post<any>(this.apiUrlHelper.login, formData);
  }

  logout() {

  }
}
