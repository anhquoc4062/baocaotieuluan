import { AuthService } from './auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {

  username: string = 'abc';
  password: string = '';
  isFailed: boolean = false;

  constructor(
    private authService: AuthService,
    private _router: Router
  ) { }

  ngOnInit() {
    $("#login").velocity("transition.slideUpIn", 1250);
    $(".row").delay(500).velocity("transition.slideLeftIn", { stagger: 500 })
  }

  login() {
    console.log(this.username);
    console.log(this.password);
    this.authService.login(this.username, this.password).subscribe(result => {
      console.log(result);
      if (result.exit_code == 0) {
        this.isFailed = true;
      } else {
        // console.log(result.data);
        const user = result.data;
        const userLocal = {
          anhDaiDien: user.anhDaiDien,
          email: user.email,
          maPhanQuyen: user.maPhanQuyen,
          maTaiKhoan: user.maTaiKhoan,
          tenTaiKhoan: user.tenTaiKhoan,
        };

        localStorage.setItem('currentUser', JSON.stringify(userLocal));
        // window.location.href = "http://localhost:4200";
        this._router.navigate(['/']);
      }
    });
  }

}
