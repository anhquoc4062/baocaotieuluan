import { OrderService } from './../../service/order.service';
import { Component, OnInit } from '@angular/core';
import { IndexdbService } from 'src/app/service/indexdb.service';
import {MessagingService} from '../../service/messaging.service';
import {HttpClient} from '@angular/common/http';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {


  constructor(
    private orderService: OrderService
  ) { }

  ngOnInit() {
    this.orderService.orderDisplay.emit({id: 'abc', name: 'bcd'});
    this.orderService.orderDisplay.subscribe((test) => console.log(test));
    this.orderService.kitchenBar2.emit(null);
    this.orderService.kitchenBar3.emit(null);
    this.orderService.kitchenBar4.emit(null);
  }

}
