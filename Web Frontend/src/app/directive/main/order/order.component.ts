import { CurrentUserService } from 'src/app/service/current-user.service';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {OrderService} from '../../../service/order.service';
import {TableService} from '../../../service/table.service';
import {HoaDon} from '../../../model/hoaDon';
import {Subscription} from 'rxjs';
import {ConstHelper} from '../../../helper/const-helper';
import {ToastService} from '../../../service/toast.service';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';

declare var $: any;

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit, OnDestroy {

  selectedTable: any = null;
  currentOrder: HoaDon = null;
  subscriptions: Subscription[] = [];
  discountPercent: number;

  constructor(
    private orderService: OrderService,
    private tableService: TableService,
    public constHelper: ConstHelper,
    private toast: ToastService,
    private currentUser: CurrentUserService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.subscriptions.push(this.tableService.isSelectedTable.subscribe(table => {
      this.selectedTable = table;
    }));
    this.subscriptions.push(this.orderService.orderDisplay.subscribe((order) => {
      if (order != undefined) {
        this.currentOrder = order;
        this.discountPercent = order.giamGia;
      } else {
        this.currentOrder = undefined;
      }
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  createOrder() {
    $('#select-product-button').trigger('click');
    this.orderService.createOrder();
  }

  updateQuantity(id, quantity) {
    if (quantity > 0) {
      this.orderService.updateQuantityOrder(id, quantity);
    }
    this.orderService.isChangedNotSave = true;
  }

  updateDiscountPercent() {
    this.orderService.preOrder.giamGia = this.discountPercent;
    this.orderService.preOrder.thanhTien = this.orderService.preOrder.tongTien - ((this.orderService.preOrder.tongTien * this.discountPercent) / 100);
    this.orderService.isChangedNotSave = true;
  }

  uploadLayout() {
    $('.list-room .mat-list-item.active').trigger('click');
    setTimeout(() => {
      this.orderService.cancelableOrder = $('.table-chair-item.active').hasClass('has-order');
    }, 300);
  }

  checkout() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        title: 'Xác nhận thanh toán',
        content: 'Bạn có muốn thanh toán order này?',
        key: 'checkout'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.orderService.preOrder.trangThai = this.constHelper.ORDER_STATUS_COMPLETE;
        this.orderService.preOrder.maThuNgan = this.currentUser.getUserID();
        this.orderService.preOrder.tenThuNgan = this.currentUser.getUserName();
        this.orderService.updateDBOrder(this.orderService.preOrder).then(() => {
          this.uploadLayout();
          this.toast.showSuccess('Thanh toán thành công');
          $('#print-order-button').trigger('click');
        });
      }
    });

  }
}
