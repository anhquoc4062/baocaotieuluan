import { ToastService } from './../../../service/toast.service';
import { GetDataService } from './../../../http-service/get-data.service';
import { ConstHelper } from './../../../helper/const-helper';
import { HangDoiHoaDon } from './../../../model/hangDoiHoaDon';
import { OrderService } from './../../../service/order.service';
import { OrderQueueService } from './../../../service/order-queue.service';
import { Component, OnInit } from '@angular/core';
import {Route, Router} from '@angular/router';
import { SyncDataService } from 'src/app/service/sync-data.service';
import { TableService } from 'src/app/service/table.service';
import { interval } from 'rxjs';
import { CurrentUserService } from 'src/app/service/current-user.service';
import { KitchenBarService } from 'src/app/kitchen-bar.service';

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  username: string = '';
  image: string = '';

  constructor(
    private route: Router,
    private syncDataService: SyncDataService,
    private orderQueueService: OrderQueueService,
    private orderService: OrderService,
    private constHelper: ConstHelper,
    private getDataService: GetDataService,
    private toast: ToastService,
    private tableService: TableService,
    private router: Router,
    private currentUser: CurrentUserService,
    private kitchenBarService: KitchenBarService
  ) { }

  ngOnInit() {
    this.setUserData();



    this.autoSync();
  }

  setUserData() {
    this.username = this.currentUser.getUserName();
    this.image = this.currentUser.getUserAvatar();
  }

  goToKitChen() {
    this.route.navigateByUrl('/kitchen-bar');
  }

  goToSale() {
    this.route.navigateByUrl('/');
  }

  loadTableDetail(tableId) {
    $('.table-content.active').removeClass('active');
    this.tableService.selectedTableId = tableId;
    this.tableService.getTableById(tableId).then(table => {
      this.tableService.isSelectedTable.emit(table);
    });
    this.orderService.getOrderByTable(tableId);
    setTimeout(() => {
      this.orderService.cancelableOrder = $('.table-chair-item.active').hasClass('has-order');
      console.log(this.orderService.cancelableOrder);
    }, 0);
  }

  uploadLayout() {
    $('#select-table-button').trigger('click');
    this.loadTableDetail(this.tableService.selectedTableId);
  }

  sync() {
    this.orderQueueService.getOrderNotSync().then((orderQueues) => {
      for (const orderQueue of orderQueues) {
        orderQueue.tinHieu = this.constHelper.QUEUE_SIGNAL_SYNCING;
        this.orderQueueService.updateOrderQueue(orderQueue);
        this.orderService.getOrderByLocalId(orderQueue['maHoaDonLocal']).then((order) => {
          console.log('order not sync', order);
          if (order != undefined) {
            this.syncDataService.syncData(order).then(ob => ob.subscribe(result => {
              if (result.exit_code == 1) {
                console.log('done');
                orderQueue.tinHieu = this.constHelper.QUEUE_SIGNAL_COMPLETE;
                this.orderQueueService.updateOrderQueue(orderQueue);
                this.toast.showSuccess('Cập nhật thành công');
              }
            }, error => {
              orderQueue.tinHieu = this.constHelper.QUEUE_SIGNAL_NEW;
              this.orderQueueService.updateOrderQueue(orderQueue);
            }));
          }
        });
      }
    });

    this.orderQueueService.getOrderFromServer().then((orderQueues) => {
      for (const orderQueue of orderQueues) {
        console.log('order not to local', orderQueue);
        this.getDataService.getOrderFromServer(orderQueue.maHoaDon).subscribe((result) => {
          console.log('data from server', result);
          if (result.data != null) {
            this.orderService.updateDBOrder(result.data, true).then(() => {
              this.uploadLayout();
              this.kitchenBarService.uploadKitchenBarLayout();
              orderQueue.tinHieu = this.constHelper.QUEUE_SIGNAL_COMPLETE;
              this.orderQueueService.updateOrderQueue(orderQueue);
            });
          }
        });
      }
    });
  }

  autoSync() {
   /*  setInterval(() => {
      console.log('autoSync');
      this.sync();
    }, 3000); */
    interval(3000).subscribe(() => {
      this.sync();
    });
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.router.navigate(['/auth']);

  }

}
