import { Component, OnInit } from '@angular/core';
import {IndexdbService} from '../../../service/indexdb.service';
import {MessagingService} from '../../../service/messaging.service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.scss']
})
export class SaleComponent implements OnInit {

  message: any;
  constructor(
    private indexDBService: IndexdbService,
    private messagingService: MessagingService,
    private http: HttpClient
  ) {

  }

  ngOnInit() {
    this.indexDBService.getData();
    const userId = 'user001';
    this.messagingService.requestPermission(userId);
    this.messagingService.receiveMessage();
    this.message = this.messagingService.currentMessage;
  }

}
