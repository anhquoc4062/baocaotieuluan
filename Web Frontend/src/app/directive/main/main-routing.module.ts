import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SaleComponent} from './sale/sale.component';
import {MainComponent} from './main.component';
import {KitchenBarComponent} from './kitchen-bar/kitchen-bar.component';
import { AuthGuard } from '../authentication/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: SaleComponent
      },
      {
        path: 'kitchen-bar',
        component: KitchenBarComponent
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
