import { OwlModule } from 'ngx-owl-carousel';
import { ApiUrlHelper } from './../../helper/api-helper';
import { NgModule } from '@angular/core';
import {AsyncPipe, CommonModule} from '@angular/common';
import { SaleComponent } from './sale/sale.component';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {MainRoutingModule} from './main-routing.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ContentComponent} from './content/content.component';
import {OrderComponent} from './order/order.component';
import {MenuComponent} from './content/menu/menu.component';
import {CategoryComponent} from './content/category/category.component';
import {FeatureComponent} from './content/feature/feature.component';
import {MatGridListModule, MatIconModule, MatListModule, MatDialogModule} from '@angular/material';
import {TimeAgoPipe} from 'time-ago-pipe';
import { KitchenBarComponent } from './kitchen-bar/kitchen-bar.component';
import {FormsModule} from '@angular/forms';
import {MessagingService} from '../../service/messaging.service';
import {HttpClient} from '@angular/common/http';
import {GetDataService} from '../../http-service/get-data.service';
import {ConstHelper} from '../../helper/const-helper';
import {LottieAnimationViewModule} from 'ng-lottie';
import { ListOrderComponent } from './kitchen-bar/list-order/list-order.component';
import { ListTableComponent } from './kitchen-bar/list-table/list-table.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';

@NgModule({
  declarations: [
    SaleComponent,
    ContentComponent,
    OrderComponent,
    MenuComponent,
    CategoryComponent,
    FeatureComponent,
    TimeAgoPipe,
    KitchenBarComponent,
    ListOrderComponent,
    ListTableComponent,
    ConfirmDialogComponent
  ],
  imports: [
    MainRoutingModule,
    PerfectScrollbarModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatDialogModule,
    MatIconModule,
    FlexLayoutModule,
    FormsModule,
    LottieAnimationViewModule.forRoot(),
    OwlModule
  ],
  providers: [
    ConstHelper,
    ApiUrlHelper
  ],
  entryComponents: [
    ConfirmDialogComponent
  ]
})
export class MainModule { }
