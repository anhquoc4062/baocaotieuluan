import { Component, OnInit } from '@angular/core';
import {OrderService} from '../../../../service/order.service';
import {ToastrService} from 'ngx-toastr';
import {ConstHelper} from '../../../../helper/const-helper';
import {ToastService} from '../../../../service/toast.service';
import {PrintHelper} from '../../../../helper/print.helper';
import {TableService} from '../../../../service/table.service';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../confirm-dialog/confirm-dialog.component';

declare var $: any;

@Component({
  selector: 'app-feature',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.scss']
})
export class FeatureComponent implements OnInit {

  constructor(
    public orderService: OrderService,
    private toast: ToastService,
    private constHelper: ConstHelper,
    private printHelper: PrintHelper,
    private tableService: TableService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
  }

  saveOrder() {
    this.orderService.updateDBOrder(this.orderService.preOrder);
    this.toast.showSuccess('Lưu hóa đơn thành công');
    this.orderService.isChangedNotSave = false;
    this.uploadLayout();
  }

  uploadLayout() {
    $('#select-table-button').trigger('click');
    setTimeout(() => {
      this.orderService.cancelableOrder = $('.table-chair-item.active').hasClass('has-order');
    }, 300);
  }
  cancelOrder() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        title: 'Xác nhận hủy order',
        content: 'Bạn có muốn hủy order này?',
        key: 'cancel'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.orderService.preOrder.trangThai = this.constHelper.ORDER_STATUS_CANCEL;
        this.orderService.updateDBOrder(this.orderService.preOrder).then(() => {
          this.toast.showSuccess('Xóa hóa đơn thành công');
          this.uploadLayout();
        });
      }
    });
  }

  printOrder() {
    this.printHelper.printOrder(this.orderService.preOrder);
  }

  changeTable() {
    this.orderService.isChangingTable = true;
    this.orderService.currentSelectedOrder = this.orderService.preOrder;
    this.tableService.selectedTableId = null;
  }
}
