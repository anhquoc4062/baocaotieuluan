import {Component, OnDestroy, OnInit} from '@angular/core';
import {RoomService} from '../../../../service/room.service';
import {CategoryService} from '../../../../service/category.service';
import {TableService} from '../../../../service/table.service';
import {ProductService} from '../../../../service/product.service';
import {Subscription} from 'rxjs';

declare var $: any;

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit, OnDestroy {

  lists: any;
  isRoomSelected: boolean = true;
  subscriptions: Subscription[] = [];
  constructor(
    public roomService: RoomService,
    public categoryService: CategoryService,
    private tableService: TableService,
    private productService: ProductService
  ) { }

  ngOnInit() {
    this.initLoadRooms();

    this.subscriptions.push(this.roomService.listDisplayRooms.subscribe(listRoom => {
      this.isRoomSelected = true;
      this.lists = listRoom;
      if (this.roomService.isSelected == null) {
        this.roomService.isSelected = 0;
        this.tableService.setDisplayTablesByRoomId(this.roomService.isSelected);
      }
    }));

    this.subscriptions.push(this.categoryService.listDisplayCategories.subscribe(async (listCategory) => {
      this.isRoomSelected = false;
      this.lists = listCategory.filter(category => category.maLoaiCha != 0);
      if (this.categoryService.isSelected == null) {
        this.categoryService.isSelected = 1;
        this.productService.setDisplayProductsByCategoryId(this.categoryService.isSelected);
      }
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  initLoadRooms() {
    console.log('load rooom');
    this.roomService.isSelected = null;
    this.roomService.setDisplayRooms();
  }

  loadRoomDetail(id) {
    $('.list-room .active').removeClass('active');
    $('.list-room .mat-list-item[data-id=' + id + ']').addClass('active');
    this.roomService.isSelected = id;
    this.tableService.setDisplayTablesByRoomId(id);
  }

  loadProductDetail(id) {
    $('.list-category .active').removeClass('active');
    $('.list-category .mat-list-item[data-id=' + id + ']').addClass('active');
    this.categoryService.isSelected = id;
    this.productService.setDisplayProductsByCategoryId(id);
  }

}
