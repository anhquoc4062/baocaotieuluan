import {Component, OnDestroy, OnInit} from '@angular/core';
import {RoomService} from '../../../../service/room.service';
import {CategoryService} from '../../../../service/category.service';
import {TableService} from '../../../../service/table.service';
import {ProductService} from '../../../../service/product.service';
import {ApiUrlHelper} from '../../../../helper/api-helper';
import {OrderService} from '../../../../service/order.service';
import {Subscription} from 'rxjs';
import {ConstHelper} from '../../../../helper/const-helper';
import {ToastService} from '../../../../service/toast.service';
import {ChiTietHoaDon} from '../../../../model/hoaDon';
import { ConfirmDialogComponent } from '../../confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';

declare var $: any;

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit, OnDestroy {

  lists: any;
  isSelectedTable: boolean = true;
  subscriptions: Subscription [] = [];
  selectedTable: any;
  constructor(
    private roomService: RoomService,
    private categoryService: CategoryService,
    public tableService: TableService,
    private productService: ProductService,
    private orderService: OrderService,
    private constHelper: ConstHelper,
    public apiUrlHelper: ApiUrlHelper,
    private toast: ToastService,
    private dialog: MatDialog
  ) {
    console.log(this.constHelper.ORDER_STATUS_CANCEL);
  }

  ngOnInit() {
    this.subscriptions.push(this.tableService.isSelectedTable.subscribe(table => {
      this.selectedTable = table;
    }));
    this.subscriptions.push(this.tableService.listDisplayTables.subscribe(list => {
      this.lists = list.filter(item => !this.constHelper.ORDER_STATUS_HIDE.includes(+item.trangThai));
    }));

    this.subscriptions.push(this.productService.listDisplayProducts.subscribe(list => {
      this.lists = list;
      this.isSelectedTable = false;
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  loadTables(event) {
    $('.menu-table-button.active').removeClass('active');
    $(event).addClass('active');
    this.roomService.setDisplayRooms();
    this.tableService.setDisplayTablesByRoomId(this.roomService.isSelected);
    this.isSelectedTable = true;
  }

  loadMenu(event) {
    $('.menu-table-button.active').removeClass('active');
    $(event).addClass('active');
    this.categoryService.setDisplayCaregories();
    this.productService.setDisplayProductsByCategoryId(this.categoryService.isSelected);
  }

  loadTableDetail(tableId) {
    $('.table-content.active').removeClass('active');
    this.tableService.selectedTableId = tableId;
    this.tableService.getTableById(tableId).then(table => {
      this.tableService.isSelectedTable.emit(table);
    });
    this.orderService.getOrderByTable(tableId);
    setTimeout(() => {
      this.orderService.cancelableOrder = $('.table-chair-item.active').hasClass('has-order') || $('.table-chair-item.active').hasClass('cooked');
      console.log(this.orderService.cancelableOrder);
    }, 0);
  }

  addOrder(productId) {
    this.orderService.addOrder(productId);
    this.orderService.isChangedNotSave = true;
  }

  cancelChangeTable() {
    this.orderService.isChangingTable = false;
    this.tableService.selectedTableId = this.orderService.preOrder.maBan;
    this.orderService.currentSelectedOrder = null;
  }

  uploadLayout() {
    $('.list-room .mat-list-item.active').trigger('click');
    setTimeout(() => {
      this.orderService.cancelableOrder = $('.table-chair-item.active').hasClass('has-order');
    }, 300);
  }

  confirmChangeTable() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: {
        title: 'Xác nhận chuyển bàn',
        content: 'Bạn có muốn chuyển bàn này?',
        key: 'cancel'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        const currentOrder = this.orderService.currentSelectedOrder;
        if (currentOrder.maBan == this.tableService.selectedTableId) {
          this.toast.showError('Không thể chuyển bàn đang chọn');
        } else {
          const currentStatus = currentOrder.trangThai;
          currentOrder.trangThai = this.constHelper.ORDER_STATUS_CHANGE;
          this.orderService.updateDBOrder(currentOrder).then(async () => {
            if (this.orderService.preOrder == undefined) {
              this.orderService.createOrderNotEmit().then(async () => {
                this.actionAfterConfirm(currentOrder, currentStatus);
              });
            } else {
              this.actionAfterConfirm(currentOrder, currentStatus);
            }
          });
        }
      }
    });

  }

  async actionAfterConfirm(currentOrder, currentStatus) {
    this.orderService.preOrder.trangThai = currentStatus;
    await currentOrder.dsMon.forEach((item: ChiTietHoaDon) => {
      this.orderService.preOrder.dsMon.push(item);
      this.orderService.preOrder.tongTien += item.donGia * item.soLuong;
    });
    this.orderService.preOrder.giamGia = currentOrder.giamGia;
    this.orderService.preOrder.thanhTien = this.orderService.preOrder.tongTien -
      ((currentOrder.giamGia * this.orderService.preOrder.tongTien) / 100);
    this.orderService.updateDBOrder(this.orderService.preOrder).then(() => {
      this.orderService.orderDisplay.emit(this.orderService.preOrder);
      this.uploadLayout();
      this.cancelChangeTable();
      this.toast.showSuccess('Chuyển bàn thành công');
    });
  }

}
