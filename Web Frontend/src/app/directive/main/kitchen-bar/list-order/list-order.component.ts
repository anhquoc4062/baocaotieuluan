import { KitchenBarService } from './../../../../kitchen-bar.service';
import { OrderService } from './../../../../service/order.service';
import { Component, OnInit } from '@angular/core';
import {ConstHelper} from '../../../../helper/const-helper';
import {ToastService} from '../../../../service/toast.service';

@Component({
  selector: 'app-list-order',
  templateUrl: './list-order.component.html',
  styleUrls: ['./list-order.component.scss']
})
export class ListOrderComponent implements OnInit {

  barDisplay: any = [null, null, null, null];

  constructor(
    private orderService: OrderService,
    private kitchenBarService: KitchenBarService,
    private constHelper: ConstHelper,
    private toast: ToastService,
  ) {}

  ngOnInit() {
    this.orderService.kitchenBar1.subscribe((order) => {
      this.barDisplay[0] = order;
    });
    this.orderService.kitchenBar2.subscribe((order) => {
      this.barDisplay[1] = order;
    });
    this.orderService.kitchenBar3.subscribe((order) => {
      this.barDisplay[2] = order;
    });
    this.orderService.kitchenBar4.subscribe((order) => {
      this.barDisplay[3] = order;
    });
  }

  cookDoneAll(index, bar) {
    console.log(bar);
    switch (index) {
      case 0:
        this.orderService.barHasOrder[0] = false;
        this.orderService.kitchenBar1.emit(null);
        break;
      case 1:
        this.orderService.barHasOrder[1] = false;
        this.orderService.kitchenBar2.emit(null);
        break;
      case 2:
        this.orderService.barHasOrder[2] = false;
        this.orderService.kitchenBar3.emit(null);
        break;
      case 3:
        this.orderService.barHasOrder[3] = false;
        this.orderService.kitchenBar4.emit(null);
        break;
    }
    bar.dsMon.forEach(item => {
      item.trangThai = this.constHelper.ORDER_STATUS_COOKED;
    });
    bar.trangThai = this.constHelper.ORDER_STATUS_COOKED;
    bar.flg_cooked = true;
    this.orderService.updateDBOrder(bar).then(() => {
      this.kitchenBarService.uploadKitchenBarLayout();
      this.toast.showSuccess(bar.tenBan + ' đã chế biến xong');
    });
  }

  cookDoneItem(index, item, bar) {
    console.log(bar);
    if (bar.dsMon.length > 1) {
      item.trangThai = this.constHelper.ORDER_STATUS_COOKED;
      this.orderService.updateDBOrder(bar).then(() => {
        bar.dsMon = bar.dsMon.filter(x => x.maChiTietLocal != item.maChiTietLocal);
        this.kitchenBarService.uploadKitchenBarLayout();
        this.toast.showSuccess(item.tenThucDon + ' đã chế biến xong');
      });
    } else {
      this.cookDoneAll(index, bar);
    }
  }
}
