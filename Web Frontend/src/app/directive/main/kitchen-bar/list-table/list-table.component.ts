import { OrderService } from './../../../../service/order.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import {ConstHelper} from '../../../../helper/const-helper';

declare var $: any;

@Component({
  selector: 'app-list-table',
  templateUrl: './list-table.component.html',
  styleUrls: ['./list-table.component.scss']
})
export class ListTableComponent implements OnInit, OnDestroy {

  listOrderServing: any;
  isClicked: any = [];

  constructor(
    private orderService: OrderService,
    private constHelper: ConstHelper
  ) {

    $('.table-item').click(function () {
      console.log('clicked');
    });
  }

  ngOnInit() {
    this.init();
    this.orderService.orderListServing.subscribe((list) => {
      this.listOrderServing = list;
    });
  }

  init() {
    this.orderService.getOrderServing().then((list) => {
      this.orderService.orderListServing.emit(list);
    });
  }

  serDisplayBar(order) {
    order.dsMon = order.dsMon.filter(item => item.trangThai != this.constHelper.ORDER_STATUS_COOKED);
    if (!$('.table-item[data-id=' + order.maHoaDon + ']').hasClass('active')) {
      if (this.orderService.barHasOrder[0] == false) {
        // $('.table-item[data-id=' + order.maHoaDon + ']').addClass('active');
        this.orderService.kitchenBar1.emit(order);
        this.orderService.barHasOrder[0] = true;
        this.isClicked[order.maHoaDon] = true;
      } else {
        if (this.orderService.barHasOrder[1] == false) {
          // $('.table-item[data-id=' + order.maHoaDon + ']').addClass('active');
          this.orderService.kitchenBar2.emit(order);
          this.orderService.barHasOrder[1] = true;
          this.isClicked[order.maHoaDon] = true;
        } else {
          if (this.orderService.barHasOrder[2] == false) {
            // $('.table-item[data-id=' + order.maHoaDon + ']').addClass('active');
            this.orderService.kitchenBar3.emit(order);
            this.orderService.barHasOrder[2] = true;
            this.isClicked[order.maHoaDon] = true;
          } else {
            if (this.orderService.barHasOrder[3] == false) {
              // $('.table-item[data-id=' + order.maHoaDon + ']').addClass('active');
              this.orderService.kitchenBar4.emit(order);
              this.orderService.barHasOrder[3] = true;
              this.isClicked[order.maHoaDon] = true;
            }
          }
        }
      }
    }
  }

  ngOnDestroy() {

  }

}
