import { OrderService } from './service/order.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class KitchenBarService {

  isSelectedOrder = {};

  constructor(
    private orderService: OrderService
  ) { }

  uploadKitchenBarLayout() {
    this.orderService.getOrderServing().then((list) => {
      this.orderService.orderListServing.emit(list);
    });
  }
}
